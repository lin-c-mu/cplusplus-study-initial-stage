#define  _CRT_SECURE_NO_WARNINGS 1

//724. 寻找数组的中心下标

class Solution
{
public:
    int pivotIndex(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> dp(n + 2);
        for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + nums[i - 1];
        for (int i = 1; i <= n; i++)
        {
            if (dp[i - 1] == dp[n] - dp[i]) return i - 1;
        }
        return -1;
    }
};