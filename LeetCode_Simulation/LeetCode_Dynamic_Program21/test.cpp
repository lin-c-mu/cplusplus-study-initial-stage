#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>
#include<unordered_map>

//1419. 数青蛙

//粗糙些
class Solution
{
public:
    int minNumberOfFrogs(string croakOfFrogs)
    {
        //1. 判断是否能构成 -- 1. 个数正确; 2. 位置正确。
        vector<int> hash(26);
        for (auto ch : croakOfFrogs)
        {
            hash[ch - 'a']++;
            //1.1 位置判断
            int a = hash['a' - 'a'], c = hash['c' - 'a'], r = hash['r' - 'a'],
                o = hash['o' - 'a'], k = hash['k' - 'a'];
            if (k > c || k > r || k > o || k > a || a > c ||
                a > r || a > o || o > c || o > r || r > c) return -1;
        }

        //1.2 个数判断
        int judge = hash[0];
        if (judge != hash['c' - 'a'] || judge != hash['r' - 'a']
            || judge != hash['o' - 'a'] || judge != hash['k' - 'a'])
            return -1;

        //2. 开始计算
        int count = 0, ret = 0;
        for (auto ch : croakOfFrogs)
        {
            if (ch == 'c') count++;
            else if (ch == 'k') count--;
            ret = max(ret, count);
        }
        return ret;
    }
};

//漂亮些
class Solution
{
public:
    int minNumberOfFrogs(string croakOfFrogs)
    {
        string t = "croak";
        int n = t.size();
        vector<int> hash(n); //使用数组模拟哈希表

        unordered_map<char, int> index; //[x, x这个字符所对应的下标]
        for (int i = 0; i < n; i++)
            index[t[i]] = i;

        for (auto ch : croakOfFrogs)
        {
            if (ch == 'c')
            {
                if (hash[n - 1] != 0) hash[n - 1]--; // 若有青蛙叫完，则重新开始叫
                hash[0]++;
            }
            else
            {
                int i = index[ch];
                if (hash[i - 1] == 0) return -1; //无前置字符
                hash[i]++; hash[i - 1]--;
            }
        }

        //判断字符剩余情况 -- 即有青蛙叫不完整
        for (int i = 0; i < n - 1; i++)
            if (hash[i] != 0) return -1;
        return hash[n - 1];
    }
};

//38. 外观数列

class Solution
{
public:
    string countAndSay(int n)
    {
        string ret = "1", copy = "1";
        while (--n)
        {
            ret = "";
            int count = 1; char num = copy[0];
            for (int i = 1; i < copy.size(); i++)
            {
                if (num == copy[i]) count++;
                else
                {
                    ret += count + '0';
                    ret += num;
                    num = copy[i];
                    count = 1;
                }
            }
            ret += count + '0';
            ret += num;
            copy = ret;
        }
        return ret;
    }
};

//6. Z 字形变换

class Solution
{
public:
    string convert(string s, int numRows)
    {
        //处理边界情况
        if (numRows == 1) return s;

        string ret;
        int d = 2 * numRows - 2, n = s.size();
        //1. 先处理第一行
        for (int i = 0; i < n; i += d)
            ret += s[i];
        //2. 处理中间行
        for (int k = 1; k < numRows - 1; k++) //枚举每一行
        {
            for (int i = k, j = d - k; i < n || j < n; i += d, j += d)
            {
                if (i < n) ret += s[i];
                if (j < n) ret += s[j];
            }
        }
        //3. 处理最后一行
        for (int i = numRows - 1; i < n; i += d)
            ret += s[i];
        return ret;
    }
};

//495. 提莫攻击

class Solution
{
public:
    int findPoisonedDuration(vector<int>& timeSeries, int duration)
    {
        int total = 0, rem = timeSeries[0] + duration - 1;
        for (int i = 1; i < timeSeries.size(); i++)
        {
            if (timeSeries[i] > rem) total += duration;
            else total += timeSeries[i] - timeSeries[i - 1];
            rem = timeSeries[i] + duration - 1;
        }
        total += duration;
        return total;
    }
};

//1576. 替换所有的问号

class Solution
{
public:
    string modifyString(string s)
    {
        string copy = '_' + s + '_';
        for (int i = 0; i < s.size(); i++)
        {
            if (s[i] == '?')
            {
                s[i] = 'a';
                while (s[i] == copy[i] || s[i] == copy[i + 2])
                    s[i]++;
                copy[i + 1] = s[i];
            }
        }
        return s;
    }
};