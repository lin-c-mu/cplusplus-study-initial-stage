#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>


//面试题 17.16. 按摩师

class Solution
{
public:
    int massage(vector<int>& nums)
    {
        //1. 创建dp表
        int n = nums.size();
        if (n == 0) return 0;
        if (n == 1) return nums[0];
        vector<int> dp(n + 1);
        //2. 初始化
        dp[1] = nums[0], dp[2] = nums[1];
        //3. 填表
        for (int i = 3; i <= n; i++)
            dp[i] = max(dp[i - 3], dp[i - 2]) + nums[i - 1];
        return max(dp[n], dp[n - 1]);
    }
};


//213. 打家劫舍 II

class Solution
{
public:
    int steal(int left, int right, vector<int>& nums)
    {
        if (left > right) return 0;
        int n = right - left + 1;
        //1. 创建dp表
        vector<int> f(n);
        auto g = f;
        //2. 初始化
        f[0] = nums[left], g[0] = 0;
        //3. 填表
        for (int i = 1; i < n; ++i)
        {
            f[i] = g[i - 1] + nums[i + left];
            g[i] = max(f[i - 1], g[i - 1]);
        }
        return max(f[n - 1], g[n - 1]);
    }
    int rob(vector<int>& nums)
    {
        int n = nums.size();
        int yes = steal(2, n - 2, nums) + nums[0];
        int no = steal(1, n - 1, nums);
        return max(yes, no);
    }
};

//740. 删除并获得点数

class Solution
{
public:
    int deleteAndEarn(vector<int>& nums)
    {
        //预处理
        vector<int> hash(1e4 + 1);
        for (auto num : nums)
            hash[num]++;
        //1. 创建dp表
        vector<int> f(1e4 + 1);
        auto g = f;
        //2. 初始化
        f[1] = hash[1];
        g[1] = 0;
        //3. 填表
        for (int i = 2; i <= 1e4; ++i)
        {
            f[i] = g[i - 1] + i * hash[i];
            g[i] = max(g[i - 1], f[i - 1]);
        }
        return max(f[1e4], g[1e4]);
    }
};

//LCR 091. 粉刷房子

class Solution
{
public:
    int minCost(vector<vector<int>>& costs)
    {
        int n = costs.size();
        //1. 创建dp表
        vector<int> red(n);
        vector<int> green(n);
        vector<int> blue(n);
        //2. 初始化
        red[0] = costs[0][0], green[0] = costs[0][1], blue[0] = costs[0][2];
        //3. 填表
        for (int i = 1; i < n; ++i)
        {
            red[i] = min(green[i - 1], blue[i - 1]) + costs[i][0];
            green[i] = min(red[i - 1], blue[i - 1]) + costs[i][1];
            blue[i] = min(red[i - 1], green[i - 1]) + costs[i][2];
        }
        //4. 返回值
        return min(min(red[n - 1], green[n - 1]), blue[n - 1]);
    }
};
