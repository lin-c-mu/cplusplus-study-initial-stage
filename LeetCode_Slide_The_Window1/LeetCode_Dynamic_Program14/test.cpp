#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//209. 长度最小的子数组

class Solution
{
public:
    int minSubArrayLen(int target, vector<int>& nums)
    {
        int n = nums.size();
        int ret = INT_MAX;
        for (int left = 0, right = 0, sum = 0; right < n; right++)
        {
            sum += nums[right];
            while (sum >= target)
            {
                ret = min(ret, right - left + 1);
                sum -= nums[left++];
            }
        }
        return ret == INT_MAX ? 0 : ret;

    }
};

//

//c
int lengthOfLongestSubstring(char* s)
{
    char* head = s;
    int max = 0;
    while (*head)
    {
        int count = 0;
        int arr[95] = { 0 };
        while (*s)
        {
            if (arr[*s - ' '] == 1)
            {
                break;
            }
            else
            {
                arr[*s - ' ']++;
                count++;
                s++;
            }
        }
        max = max > count ? max : count;
        s = ++head;
    }
    return max;
}

//c++
class Solution
{
public:
    int lengthOfLongestSubstring(string s)
    {
        int n = s.size(), ret = 0;
        vector<int> hash(128); // 使用数组模拟哈希表
        for (int left = 0, right = 0; right < n; right++)
        {
            hash[s[right]]++;
            while (hash[s[right]] > 1)
                hash[s[left++]]--;
            ret = max(ret, right - left + 1);
        }
        return ret;
    }
};

//1004. 最大连续1的个数 III

class Solution
{
public:
    int longestOnes(vector<int>& nums, int k)
    {
        int n = nums.size(), ret = 0;
        for (int left = 0, right = 0, zero = 0; right < n; right++)
        {
            if (nums[right] == 0) zero++;
            while (zero > k)
                if (nums[left++] == 0) zero--;
            ret = max(ret, right - left + 1);
        }
        return ret;
    }
};

//1658. 将 x 减到 0 的最小操作数

class Solution
{
public:
    int minOperations(vector<int>& nums, int x)
    {
        int sum = 0;
        for (int a : nums) sum += a;
        int target = sum - x;
        int ret = -1;
        //细节处理
        if (target < 0) return ret;

        int n = nums.size();
        for (int left = 0, right = 0, tmp = 0; right < n; right++)
        {
            tmp += nums[right];//进窗口
            while (tmp > target) //判断
                tmp -= nums[left++]; //出窗口

            if (target == tmp)  // 跟新结果
                ret = max(ret, right - left + 1);
        }
        if (ret == -1) return ret;
        else return n - ret;
    }
};