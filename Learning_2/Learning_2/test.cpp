#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

//struct ListNode
//{
//	ListNode* _next;
//	int _val;
//
//	ListNode(int val)
//		:_next(nullptr)
//		,_val(val)
//	{
//
//	}
//};
//
////  c语言，创建链表节点
////struct ListNode* CreatNode(int val)
////{
////	struct ListNode* newnode = (struct ListNode*)malloc(sizeof(struct ListNode));
////	if (newnode == NULL)
////	{
////		perror("malloc fail");
////		exit(-1);
////	}
////	newnode->val = val;
////	newnode->_next = NULL;
////	return newnode;
////}
//
////  C++版，创建链表
//ListNode* CreatListNode(int n)
//{
//	ListNode head(-1); //哨兵位
//	ListNode* tail = &head;
//	int val;
//	printf("请依次输入 %d 个节点的值:>", n);
//	for (size_t i = 0; i < n; i++)
//	{
//		cin >> val;
//		tail->_next = new ListNode(val);
//		tail = tail->_next;
//	}
//	return head._next;
//}
//
//void func()
//{
//	int n = 1;
//	while (1)
//	{
//		int* p = new int[1024 * 1024 * 100];
//		cout << n << " -> " << p << endl;
//		++n;
//	}
//}
//
//int main()
//{
//	int* p0 = (int*)malloc(sizeof(int) * 10);
//	free(p0);
//
//	//1、用法上，变简洁了
//	int* p1 = new int;
//	int* p2 = new int[10]; // new十个int对象
//	delete p1;
//	delete[] p2;
//
//	//2、可以控制初始化
//	int* p3 = new int(10); // new一个int对象， 初始化为10
//	int* p4 = new int[10] {1, 2, 3, 4}; //其余默认为0
//	delete p3;
//	delete[] p4;
//
//	//3、自定义类型，开空间+构造函数
//	//4、new失败了后会抛异常，不需要手动检查
//	ListNode* node1 = new ListNode(1);
//	ListNode* node2 = new ListNode(2);
//	ListNode* node3 = new ListNode(3);
//	delete node1;
//	delete node2;
//	delete node3;
//
//	//ListNode* list = CreatListNode(5);
//
//	//new开辟失败，抛异常演示
//	//   bad allocation
//
//	/*try
//	{
//		func();
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}*/
//	return 0;
//}

class A
{
public:
	A(int a = 1)
		:_a(a)
	{
		cout << this << "->A(int a = 1)" << endl;
	}
	~A()
	{
		cout << this << "->~A()" << endl;
	}
private:
	int _a;
};

int main()
{
	//事实上 new 底层就是 operator new + 构造函数 ，而 operator new 底层又是 malloc ！
	//new -> operator new + 构造函数 -> malloc + 构造函数
	//至于 new[n] ->  operator new[] + n次构造函数 -> operator new + n次构造函数 -> malloc + n次构造函数
	//抛异常是在 operator new 这一层
	A* ptr = new A[10];
	delete[] ptr;
	//一定要，匹配使用new  delete

	A* p1 = (A*)operator new(sizeof(A));
	//显式调用构造函数对一块已有的空间初始化
	new(p1)A(1); // 注意：如果A类的构造函数有参数时，此处需要传参。 定位new调用构造函数初始化
	p1->~A();
	operator delete(p1);
	//事实上平时不这么用
	return 0;
}