#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//1137. 第 N 个泰波那契数

class Solution
{
public:
    int tribonacci(int n)
    {
        //1. 创建dp表
        //2. 初始化
        //3. 填表
        //4. 返回结束

        if (n == 0) return 0;
        if (n == 1 || n == 2) return 1;

        vector<int> dp(n + 1);
        dp[0] = 0, dp[1] = dp[2] = 1;

        for (int i = 3; i <= n; ++i)
            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
        return dp[n];
    }
};

//面试题 08.01. 三步问题

class Solution
{
public:
    int waysToStep(int n)
    {
        //1. 创建dp表
        //2. 初始化
        //3. 填表
        //4. 返回结束

        if (n == 1 || n == 2) return n;
        if (n == 3) return 4;
        vector<int> dp(n + 1);
        dp[1] = 1, dp[2] = 2, dp[3] = 4;

        int num = 1e9 + 7;
        for (int i = 4; i <= n; ++i)
            dp[i] = ((dp[i - 1] + dp[i - 2]) % num + dp[i - 3]) % num;

        return dp[n];
    }
};

//746. 使用最小花费爬楼梯

class Solution
{
public:
    int minCostClimbingStairs(vector<int>& cost)
    {
        //1. 创建dp表
        //2. 初始化
        //3. 填表
        //4. 返回结束

        int n = cost.size();
        vector<int> dp(n + 1);
        dp[0] = 0, dp[1] = 0;

        for (int i = 2; i <= n; ++i)
            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
        return dp[n];
    }
};

//91. 解码方法

class Solution {
public:
    int numDecodings(string s) {
        int n = s.size();
        vector<int> f(n + 1);
        f[0] = 1;
        for (int i = 1; i <= n; ++i) {
            if (s[i - 1] != '0') {
                f[i] += f[i - 1];
            }
            if (i > 1 && s[i - 2] != '0' && ((s[i - 2] - '0') * 10 + (s[i - 1] - '0') <= 26)) {
                f[i] += f[i - 2];
            }
        }
        return f[n];
    }
};
