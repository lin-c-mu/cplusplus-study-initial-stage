#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>
#include<unordered_map>

//873. 最长的斐波那契子序列的长度

class Solution
{
public:
    int lenLongestFibSubseq(vector<int>& arr)
    {
        int n = arr.size();
        vector<vector<int>> dp(n, vector<int>(n, 2));
        //优化
        unordered_map <int, int> hash;
        for (int i = 0; i < n; i++)
            hash[arr[i]] = i;

        int ans = 2;
        for (int j = 2; j < n; j++) //固定最后一个位置
            for (int i = 1; i < j; i++) // 固定倒数第二个位置
            {
                int a = arr[j] - arr[i];
                if (a < arr[i] && hash.count(a)) dp[i][j] = dp[hash[a]][i] + 1;
                ans = max(ans, dp[i][j]);
            }
        return ans < 3 ? 0 : ans;
    }
};

//1027 最长等差数列

class Solution
{
public:
    int longestArithSeqLength(vector<int>& nums)
    {
        int n = nums.size();
        vector<vector<int>> dp(n, vector<int>(n, 2)); // 创建dp表 + 初始化
        unordered_map<int, int> hash;

        hash[nums[0]] = 0;
        int ans = 2;
        for (int i = 1; i < n; i++) // 固定倒数第二个数
        {
            for (int j = i + 1; j < n; j++)  // 枚举倒数第一个数
            {
                int a = 2 * nums[i] - nums[j];
                if (hash.count(a)) dp[i][j] = dp[hash[a]][i] + 1;
                ans = max(ans, dp[i][j]);
            }
            hash[nums[i]] = i;
        }
        return ans;
    }
};

//446. 等差数列划分 II - 子序列

class Solution
{
public:
    int numberOfArithmeticSlices(vector<int>& nums)
    {
        int n = nums.size();
        vector<vector<int>> dp(n, vector<int>(n));
        //优化
        unordered_map<long long, vector<int>> hash;
        for (int i = 0; i < n; i++) hash[nums[i]].push_back(i);

        int ans = 0;
        for (int i = 1; i < n - 1; i++) // 固定第一个数
        {
            for (int j = i + 1; j < n; j++) // 固定第二个数
            {
                long long a = (long long)2 * nums[i] - nums[j];
                if (hash.count(a))
                {
                    for (auto k : hash[a])
                    {
                        if (k < i) dp[i][j] += dp[k][i] + 1;
                    }
                }
                ans += dp[i][j];
            }
        }
        return ans;
    }
};