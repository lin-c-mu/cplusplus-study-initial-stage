#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//面试题 01.01. 判定字符是否唯一

class Solution
{
public:
    bool isUnique(string astr)
    {
        //判断 - 鸽巢法
        if (astr.size() > 26) return false;
        //位图
        int bitMap = 0;
        //判断、
        for (int i = 0; i < astr.size(); i++)
        {
            int k = astr[i] - 'a';
            if (((bitMap >> k) & 1) == 1) return false;
            bitMap = bitMap | (1 << k);
        }
        return true;
    }
};

//268. 丢失的数字

//高斯求和
class Solution
{
public:
    int missingNumber(vector<int>& nums)
    {
        int n = nums.size();
        int sum = n * (1 + n) / 2;
        int sum_nums = 0;
        for (int num : nums) sum_nums += num;
        return sum - sum_nums;
    }
};

//位运算
class Solution
{
public:
    int missingNumber(vector<int>& nums)
    {
        int n = nums.size();
        int anr = 0;
        for (int i = 0; i < n; i++)
            anr = anr ^ i ^ nums[i];
        return anr ^ n;
    }
};

//371. 两整数之和

class Solution
{
public:
    int getSum(int a, int b)
    {

        while (b != 0)
        {
            int x = a ^ b; //无进位相加
            size_t y = ((size_t)(a & b) << 1); //找到进位
            a = x;
            b = y;
        }
        return a;
    }
};

//137. 只出现一次的数字 II

class Solution
{
public:
    int singleNumber(vector<int>& nums)
    {
        int a = 0, b = 0;
        for (int num : nums)
        {
            b = ~a & (b ^ num);
            a = ~b & (a ^ num);
        }
        return b;
    }
};

class Solution
{
public:
    int singleNumber(vector<int>& nums)
    {
        int anr = 0;
        for (int i = 0; i < 32; i++)
        {
            int sum = 0;
            for (int n : nums)
            {
                sum += (n >> i) & 1;
            }
            anr ^= (sum % 3) << i;
        }
        return anr;
    }
};

//面试题 17.19. 消失的两个数字

class Solution
{
public:
    vector<int> missingTwo(vector<int>& nums)
    {
        int anr = 0, n = nums.size();
        for (int i = 1; i <= n; i++)
            anr = anr ^ nums[i - 1] ^ i;
        anr = anr ^ (n + 1) ^ (n + 2);
        vector<int> type;
        anr = (anr == INT_MIN ? anr : anr & (-anr));
        int type1 = 0, type2 = 0;
        for (int num = 1; num <= n + 2; num++)
        {
            if (num & anr)
                type1 ^= num;
            else
                type2 ^= num;
        }
        for (int i = 0; i < n; i++)
        {
            if (nums[i] & anr)
                type1 ^= nums[i];
            else
                type2 ^= nums[i];
        }
        return { type1, type2 };
    }
};

class Solution
{
public:
    vector<int> missingTwo(vector<int>& nums)
    {
        //1. 将所有的数异或在一起
        int tmp = 0;
        for (auto x : nums) tmp ^= x;
        for (int i = 1; i <= nums.size() + 2; i++) tmp ^= i;

        //2. 找出 a, b 中比特位不同的那一位
        int diff = 0;
        while (1)
        {
            if (((tmp >> diff) & 1) == 1) break;
            else diff++;
        }

        //3. 根据 diff 位的不同，将所有的数划分为两类来异或
        int a = 0, b = 0;
        for (int x : nums)
            if (((x >> diff) & 1) == 1) b ^= x;
            else a ^= x;
        for (int i = 1; i <= nums.size() + 2; i++)
            if (((i >> diff) & 1) == 1) b ^= i;
            else a ^= i;
        return { a, b };
    }
};