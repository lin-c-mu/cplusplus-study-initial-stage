#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<assert.h>

namespace lzw
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		iterator begin()
		{
			return _start;
		}

		const_iterator begin() const
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator end() const
		{
			return _finish;
		}

		vector()
		{

		}

		//构造
		vector(int n, const T& val = T())
		{
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}
		
		//析构
		~vector()
		{
			delete[] _start;
		}

		//大小
		size_t size() const
		{
			return _finish - _start;
		}

		//容量
		size_t capacity() const
		{
			return _endofstorage - _start;
		}

		// 下标 [] 访问
		T& operator[](size_t pos)
		{
			assert(pos < size());
			return *(_start + pos);
		}

		// reserve() 开辟空间
		void reserve(size_t n)
		{
			//大于 size 才会扩
			if (n > size())
			{
				int old_size = _finish - _start;
				delete[] _start;
				_start = new T[n];
				_finish = _start + old_size;
				_endofstorage = _start + n;
			}
		}

		//尾插
		void push_back(const T& x)
		{
			if (_finish == _endofstorage)
			{
				reserve(capacity() == 0 ? 4 : capacity() * 2);
			}
			*_finish = x;
			++_finish;
		}

	private:
		iterator _start = nullptr;
		iterator _finish = nullptr;
		iterator _endofstorage = nullptr;
	};

	void test_vector1()
	{
		vector<int> v1(10, 2);
		v1.push_back(11);
		v1.push_back(22);
		v1.push_back(33);
		v1.push_back(44);

		for (auto a : v1)
			cout << a << ' ';
	}

};

