#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//704. 二分查找

//标准二分查找
class Solution
{
public:
    int search(vector<int>& nums, int target)
    {
        int left = 0, right = nums.size() - 1;
        while (left <= right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] > target) right = mid - 1;
            else if (nums[mid] < target) left = mid + 1;
            else return mid;
        }
        return -1;
    }
};

//34. 在排序数组中查找元素的第一个和最后一个位置

class Solution
{
public:
    vector<int> searchRange(vector<int>& nums, int target)
    {
        //处理边界情况
        if (nums.size() == 0) return { -1, -1 };

        int left = 0, right = nums.size() - 1;
        //找左值
        int begin;
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] < target) left = mid + 1;
            else right = mid;
        }
        if (nums[left] != target) return { -1, -1 };
        begin = left;
        //找右值
        int end;
        right = nums.size() - 1;
        while (left < right)
        {
            int mid = left + (right - left + 1) / 2;
            if (nums[mid] > target) right = mid - 1;
            else left = mid;
        }
        end = right;
        return { begin, end };
    }
};