#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//647. 回文子串

class Solution
{
public:
    int countSubstrings(string s)
    {
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));

        int ans = 0;
        for (int i = n - 1; i >= 0; i--)
        {
            for (int j = i; j < n; j++)
            {
                if (s[i] == s[j] && (i + 1 >= j || dp[i + 1][j - 1]))
                {
                    ans++;
                    dp[i][j] = true;
                }
            }
        }
        return ans;
    }
};

//5. 最长回文子串

class Solution
{
public:
    string longestPalindrome(string s)
    {
        int n = s.size();
        vector<vector<int>> dp(n, vector<int>(n));

        int left = 0, right = 0, sum = 0;
        for (int i = n - 1; i >= 0; i--)
        {
            for (int j = i; j < n; j++)
            {
                if (s[i] == s[j])
                {
                    if (i == j) dp[i][j] = 1;
                    else if (i + 1 == j) dp[i][j] = 2;
                    else if (dp[i + 1][j - 1]) dp[i][j] = dp[i + 1][j - 1] + 2;
                }
                if (sum < dp[i][j])
                {
                    sum = dp[i][j];
                    left = i, right = j;
                }
            }
        }

        string ans;
        for (int i = left; i <= right; i++) ans += s[i];
        return ans;
    }
};

//1745. 分割回文串 IV

class Solution
{
public:
    bool checkPartitioning(string s)
    {
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));

        //1. 处理子串是否为回文字符串
        for (int i = n - 1; i >= 0; i--)
        {
            for (int j = i; j < n; j++)
            {
                if (s[i] == s[j])
                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
            }
        }

        //2.枚举所有的第二个字符串的起始位置和结束位置
        for (int i = 0; i < n - 2; i++)
            for (int j = i + 1; j < n - 1; j++)
                if (dp[0][i] && dp[i + 1][j] && dp[j + 1][n - 1])
                    return true;
        return false;
    }
};

//132. 分割回文串 II

class Solution
{
public:
    int minCut(string s)
    {
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));

        //1. 处理子串是否为回文字符串
        for (int i = n - 1; i >= 0; i--)
        {
            for (int j = i; j < n; j++)
            {
                if (s[i] == s[j])
                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
            }
        }

        //2. 
        vector<int> count(n, INT_MAX);
        for (int i = 0; i < n; i++)
        {
            if (dp[0][i]) count[i] = 0;
            else
            {
                for (int j = 1; j <= i; j++)
                    if (dp[j][i]) count[i] = min(count[j - 1] + 1, count[i]);
            }
        }
        return count[n - 1];
    }
};

//516. 最长回文子序列

class Solution
{
public:
    int longestPalindromeSubseq(string s)
    {
        int n = s.size();
        vector<vector<int>> dp(n, vector<int>(n));

        for (int i = n - 1; i >= 0; i--)
        {
            dp[i][i] = 1;
            for (int j = i + 1; j < n; j++)
            {
                if (s[i] == s[j]) dp[i][j] = dp[i + 1][j - 1] + 2;
                else dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
            }
        }
        return dp[0][n - 1];
    }
};

//1312. 让字符串成为回文串的最少插入次数

class Solution {
public:
    int minInsertions(string s) {
        int n = s.size();
        string t(s.rbegin(), s.rend());
        vector<vector<int>> dp(n + 1, vector<int>(n + 1));
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= n; ++j) {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
                if (s[i - 1] == t[j - 1]) {
                    dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + 1);
                }
            }
        }
        return n - dp[n][n];
    }
};

