#define  _CRT_SECURE_NO_WARNINGS 1

//125. 验证回文串



class Solution
{
public:
    bool judgeS(char s)
    {
        if (s < 'A' && s > '9')
            return false;
        else if (s < 'a' && s > 'Z')
            return false;
        else if (s < '0' || s > 'z')
            return false;
        else
            return true;
    }
    bool isPalindrome(string s)
    {
        int begin = 0, end = s.size();
        while (begin < s.size())
        {
            while (begin < s.size() && !judgeS(s[begin]))
                ++begin;
            while (end >= 0 && !judgeS(s[end]))
                --end;
            if (begin < s.size() && tolower(s[begin]) != tolower(s[end]))
            {
                return false;
            }
            ++begin;
            --end;
        }
        return true;
    }
};

//387. 字符串中的第一个唯一字符

class Solution
{
public:
    int firstUniqChar(string s)
    {
        int arr[26];
        memset(arr, 0, sizeof(arr));
        string::iterator it = s.begin();
        while (it != s.end())
        {
            ++arr[*it - 'a'];
            ++it;
        }
        it = s.begin();
        int count = 0;
        while (it != s.end())
        {
            if (arr[*it - 'a'] == 1)
                return count;
            ++count;
            ++it;
        }
        return -1;
    }
};