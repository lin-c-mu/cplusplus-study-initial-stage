#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>
#include<unordered_map>

//724. 寻找数组的中心下标

class Solution
{
public:
    int pivotIndex(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> dp(n + 2);
        for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + nums[i - 1];
        for (int i = 1; i <= n; i++)
        {
            if (dp[i - 1] == dp[n] - dp[i]) return i - 1;
        }
        return -1;
    }
};

//238. 除自身以外数组的乘积

class Solution
{
public:
    vector<int> productExceptSelf(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> left(n + 1, 1);
        auto right = left;
        //left -> 前i个数的乘积; right -> 后n - i个数的乘积
        for (int i = 1; i <= n; i++)
        {
            left[i] = left[i - 1] * nums[i - 1];
            right[n - i] = right[n - i + 1] * nums[n - i];
        }
        vector<int> anr(n);
        for (int i = 1; i <= n; i++) anr[i - 1] = left[i - 1] * right[i];
        return anr;
    }
};

//560. 和为 K 的子数组

//暴解
class Solution
{
public:
    int subarraySum(vector<int>& nums, int k)
    {
        int n = nums.size();
        vector<int> dp(n + 1);
        //1.前缀和
        for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + nums[i - 1];
        //2.判断左右两区间
        int count = 0;
        for (int i = 1; i <= n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (dp[i] - dp[j] == k) count++;
            }
        }
        return count;
    }
};

//前缀和 + 哈希表
class Solution
{
public:
    int subarraySum(vector<int>& nums, int k)
    {
        unordered_map<int, int> hash; //统计前缀和出现的次数
        hash[0] = 1; //防止[0, i]为0的情况

        int sum = 0, count = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            sum += nums[i];//计算当前位置的前缀和
            int mul = sum - k;
            if (hash[mul]) count += hash[mul]; // 统计个数
            hash[sum]++;
        }
        return count;
    }
};

//974. 和可被 K 整除的子数组

class Solution
{
public:
    int subarraysDivByK(vector<int>& nums, int k)
    {
        unordered_map<int, int> hash;
        hash[0] = 1; //这个数就是余数

        int count = 0, sum = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            sum += nums[i];
            count += hash[(sum % k + k) % k];
            hash[(sum % k + k) % k]++; //修正后的值
        }
        return count;
    }
};

//525. 连续数组

class Solution
{
public:
    int findMaxLength(vector<int>& nums)
    {
        //1.将0修改为-1
        for (int i = 0; i < nums.size(); i++)
            if (nums[i] == 0) nums[i] = -1;
        //2.找和为0的最长数组
        unordered_map<int, int> hash; //和 -> 下标
        int sum = 0, len = 0;
        for (int i = 0; i < nums.size(); i++)
        {
            sum += nums[i];
            if (hash[sum] == 0 && sum) hash[sum] = i + 1;
            len = max(len, i - hash[sum] + 1);
        }
        return len;
    }
};

//1314. 矩阵区域和

class Solution
{
public:
    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k)
    {
        int col = mat.size(), row = mat[0].size();
        //1.前缀和
        vector<vector<int>> dp(col + 1, vector<int>(row + 1));
        for (int i = 1; i <= col; i++)
        {
            for (int j = 1; j <= row; j++)
            {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + mat[i - 1][j - 1];
            }
        }
        //确定范围，求值
        vector<vector<int>> anr(col, vector<int>(row));
        for (int i = 1; i <= col; i++)
        {
            for (int j = 1; j <= row; j++)
            {
                int l1 = i - k < 1 ? 1 : i - k;
                int r1 = j - k < 1 ? 1 : j - k;
                int l2 = i + k > col ? col : i + k;
                int r2 = j + k > row ? row : j + k;
                anr[i - 1][j - 1] = dp[l2][r2] - dp[l1 - 1][r2] - dp[l2][r1 - 1] + dp[l1 - 1][r1 - 1];
            }
        }
        return anr;
    }
};