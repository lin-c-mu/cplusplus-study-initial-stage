#define  _CRT_SECURE_NO_WARNINGS 1

//2960. 统计已测试设备

//方法一：模拟
class Solution {
public:
    int countTestedDevices(vector<int>& batteryPercentages) {
        int n = batteryPercentages.size();
        int need = 0;
        for (int i = 0; i < n; i++) {
            if (batteryPercentages[i] > 0) {
                need++;
                for (int j = i + 1; j < n; j++) {
                    batteryPercentages[j] = max(batteryPercentages[j] - 1, 0);
                }
            }
        }
        return need;
    }
};

//方法二：差分
class Solution {
public:
    int countTestedDevices(vector<int>& batteryPercentages) {
        int need = 0;
        for (int battery : batteryPercentages) {
            if (battery > need) {
                need++;
            }
        }
        return need;
    }
};
