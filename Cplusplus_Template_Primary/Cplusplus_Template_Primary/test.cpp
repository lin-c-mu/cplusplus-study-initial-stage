#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

////C语言中的两数交换
//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//void Swap(double& left, double& right)
//{
//	double temp = left;
//	left = right;
//	right = temp;
//}
//void Swap(char& left, char& right)
//{
//	char temp = left;
//	left = right;
//	right = temp;
//}
////......
//
//template<class T>
//void Swap(const T& a, const T& b)
//{
//	const T tmp = a;
//	a = b;
//	b = a;
//}


// 2. 函数模板

//template<class T>
//T Add(const T& left, const T& right)
//{
//	return left + right;
//}
//
//int main()
//{
//	int a1 = 10, a2 = 20;
//	double b1 = 10.1, b2 = 20.2;
//	std::cout << Add(a1, a2) << std::endl;
//	std::cout << Add(b1, b2) << std::endl;
//
//	//Add(a1, b1); -> 不能通过编译
//	std::cout << Add(a1, (int)b1) << std::endl;
//	std::cout << Add<int>(a1, b1) << std::endl;     //显示实例化 T 为 int
//	std::cout << Add<double>(a1, b1) << std::endl;  //显示实例化 T 为 double
//	return 0;
//}

//// 专门处理int的加法函数
//int Add(int left, int right)
//{
//	return left + right;
//}
//// 通用加法函数
//template<class T1, class T2>
//T1 Add(T1 left, T2 right)
//{
//	return left + right;
//}
////// 通用加法函数
////template<class T>
////T Add(T left, T right)
////{
////	return left + right;
////}
//
//void Test()
//{
//	Add(1, 2); // 与非函数模板类型完全匹配，不需要函数模板实例化
//	Add(1, 2.0); // 模板函数可以生成更加匹配的版本，编译器根据实参生成更加匹配的Add函数
//}


//// 专门处理int的加法函数
//int Add(int left, int right)
//{
//	return left + right;
//}
//
//template<class T>
//T Add(T left, T right)
//{
//	return left + right;
//}
//int main()
//{
//	Add(1, 2);       //调用非模板函数
//	Add(1, 2.0);     // 隐式类型转换，调用非模板函数
//	Add(1.1, 2.2);   // 调用模板函数
//	return 0;
//}


//// Vector类名， Vector<int>才是类型
//Vector<int> s1;
//Vector<double> s2;

//3. 类模板

//template<class T1, class T2, ..., class Tn>
//class 类模板名
//{
//	// 类内成员定义
//};



// 动态顺序表
// 注意：Vector不是具体的类，是编译器根据被实例化的类型生成具体类的模具
template<class T>
class Vector
{
public:
	Vector(size_t capacity = 10)
		: _pData(new T[capacity])
		, _size(0)
		, _capacity(capacity)
	{}

	// 使用析构函数演示：在类中声明，在类外定义。
	~Vector();

	void PushBack(const T& data)；
		void PopBack()；
		// ...

		size_t Size() { return _size; }

	T& operator[](size_t pos) 
	{
		assert(pos < _size);
		return _pData[pos];
	}

private:
	T* _pData;
	size_t _size;
	size_t _capacity;
};
// 注意：类模板中函数放在类外进行定义时，需要加模板参数列表
template <class T>
Vector<T>::~Vector()
{
	if (_pData)
		delete[] _pData;
	_size = _capacity = 0;
}
