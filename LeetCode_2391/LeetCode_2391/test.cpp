#define  _CRT_SECURE_NO_WARNINGS 1

//2391. 收集垃圾的最少总时间

public:
    int garbageCollection(vector<string>& garbage, vector<int>& travel) {
        unordered_map<char, int> distance;
        int res = 0, cur_dis = 0;
        for (int i = 0; i < garbage.size(); i++) {
            res += garbage[i].size();
            if (i > 0) {
                cur_dis += travel[i - 1];
            }
            for (auto c : garbage[i]) {
                distance[c] = cur_dis;
            }
        }
        for (auto& [k, v] : distance) {
            res += v;
        }
        return res;
    }
};