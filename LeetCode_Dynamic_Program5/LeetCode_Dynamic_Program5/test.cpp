#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//714. 买卖股票的最佳时机含手续费

class Solution
{
public:
    int maxProfit(vector<int>& prices, int fee)
    {
        int n = prices.size();
        //1.创建dp表
        vector<vector<int>> dp(n, vector<int>(2));
        //2. 初始化
        dp[0][0] = 0, dp[0][1] = -prices[0];
        //3.填表
        for (int i = 1; i < n; ++i)
        {
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] + prices[i] - fee);
            dp[i][1] = max(dp[i - 1][0] - prices[i], dp[i - 1][1]);
        }
        //4. 返回值
        return dp[n - 1][0];
    }
};

//309. 买卖股票的最佳时机含冷冻期

class Solution
{
public:
    int maxProfit(vector<int>& prices)
    {
        int n = prices.size();
        //1. 创建dp表
        vector<vector<int>> dp(n, vector<int>(3));
        //2. 初始化
        dp[0][0] = dp[0][2] = 0, dp[0][1] = -prices[0];
        //3. 填表
        for (int i = 1; i < n; ++i)
        {
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][2]);
            dp[i][1] = max(dp[i - 1][0] - prices[i], dp[i - 1][1]);
            dp[i][2] = dp[i - 1][1] + prices[i];
        }
        //4. 返回值
        return max(dp[n - 1][0], dp[n - 1][2]);
    }
};