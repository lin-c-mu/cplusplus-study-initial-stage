#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//62. 不同路径

class Solution
{
public:
    int uniquePaths(int m, int n)
    {
        //1. 创建dp表
        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
        //2. 初始化
        dp[0][1] = 1;
        //3. 填表
        //从上到下填表 -> 从左到右填表
        for (int i = 1; i <= m; ++i)
            for (int j = 1; j <= n; ++j)
                dp[i][j] = dp[i][j - 1] + dp[i - 1][j];
        //4. 返回值
        return dp[m][n];
    }
};

//63. 不同路径 II

class Solution
{
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid)
    {
        int row = obstacleGrid.size(), col = obstacleGrid[0].size();
        //1. 创建dp表
        vector<vector<int>> dp(row + 1, vector<int>(col + 1));
        //2. 初始化
        dp[0][1] = 1;
        //3. 填表
        //从上到下填表 -> 从左到右填表
        for (int i = 1; i <= row; ++i)
        {
            for (int j = 1; j <= col; ++j)
            {
                if (obstacleGrid[i - 1][j - 1] == 1)
                    dp[i][j] = 0;
                else
                    dp[i][j] = dp[i][j - 1] + dp[i - 1][j];
            }
        }
        //4. 返回值
        return dp[row][col];
    }
};


//LCR 166. 珠宝的最高价值

class Solution
{
public:
    int jewelleryValue(vector<vector<int>>& frame)
    {
        int row = frame.size(), col = frame[0].size();
        //1. 创建dp表
        vector<vector<int>> dp(row + 1, vector<int>(col + 1));
        //2. 初始化
        //3. 填表
        for (int i = 1; i <= row; ++i)
            for (int j = 1; j <= col; ++j)
                dp[i][j] = frame[i - 1][j - 1] + max(dp[i - 1][j], dp[i][j - 1]);
        //4. 返回值
        return dp[row][col];
    }
};