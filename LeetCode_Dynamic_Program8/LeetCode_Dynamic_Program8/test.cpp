#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//1567. 乘积为正数的最长子数组长度

class Solution
{
public:
    int getMaxLen(vector<int>& nums)
    {
        int n = nums.size();
        //1. 创建dp表 f->正; g->负
        vector<int> f(n + 1);
        auto g = f;
        //2. 初始化
        //3. 填表
        for (int i = 1; i <= n; ++i)
        {
            if (nums[i - 1] > 0)
            {
                f[i] = f[i - 1] + 1;
                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
            }
            else if (nums[i - 1] < 0)
            {
                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
                g[i] = f[i - 1] + 1;
            }
            else
            {
                g[i] = f[i] = 0;
            }
        }
        //4. 返回值
        int ans = 0;
        for (int i = 1; i <= n; ++i)
            ans = max(ans, f[i]);
        return ans;
    }
};

//413. 等差数列划分

class Solution
{
public:
    int numberOfArithmeticSlices(vector<int>& nums)
    {
        int n = nums.size();
        //1. 创建dp表 f->个数; g->差值
        vector<int> f(n);
        vector<int> g(n, INT_MIN);
        //处理差值
        for (int i = 1; i < n; ++i)
            g[i] = nums[i] - nums[i - 1];
        //2. 初始化
        //3. 填表
        for (int i = 1; i < n; ++i)
        {
            if (g[i] == g[i - 1]) f[i - 1] = f[i - 2] + 1;
            else f[i - 1] = 0;
        }
        //4. 返回值
        int ans = 0;
        for (int i = 0; i < n; ++i)
            ans += f[i];
        return ans;
    }
};

//978. 最长湍流子数组

class Solution
{
public:
    int maxTurbulenceSize(vector<int>& arr)
    {
        int n = arr.size();
        //1. 创建dp表
        vector<int> dp(n + 1);
        auto dp2 = dp;
        //2. 初始化
        //3. 填表
        for (int i = 0; i < n - 1; ++i)
        {
            if (dp[i] == 0)
            {
                //奇数
                if (i % 2 == 1)
                {
                    if (arr[i] > arr[i + 1]) dp[i + 1] = 1;
                    else dp[i + 1] = 0;
                }
                //偶数
                else
                {
                    if (arr[i] < arr[i + 1]) dp[i + 1] = 1;
                    else dp[i + 1] = 0;
                }
            }
            else
            {
                //奇数
                if (i % 2 == 1)
                {
                    if (arr[i] > arr[i + 1] && arr[i] > arr[i - 1]) dp[i + 1] = dp[i] + 1;
                    else dp[i + 1] = 0;
                }
                //偶数
                else
                {
                    if (arr[i] < arr[i + 1] && arr[i] < arr[i - 1]) dp[i + 1] = dp[i] + 1;
                    else dp[i + 1] = 0;
                }
            }
        }
        for (int i = 0; i < n - 1; ++i)
        {
            if (dp2[i] == 0)
            {
                //奇数
                if (i % 2 == 1)
                {
                    if (arr[i] < arr[i + 1]) dp2[i + 1] = 1;
                    else dp2[i + 1] = 0;
                }
                //偶数
                else
                {
                    if (arr[i] > arr[i + 1]) dp2[i + 1] = 1;
                    else dp2[i + 1] = 0;
                }
            }
            else
            {
                //奇数
                if (i % 2 == 1)
                {
                    if (arr[i] < arr[i + 1] && arr[i] < arr[i - 1]) dp2[i + 1] = dp2[i] + 1;
                    else dp2[i + 1] = 0;
                }
                //偶数
                else
                {
                    if (arr[i] > arr[i + 1] && arr[i] > arr[i - 1]) dp2[i + 1] = dp2[i] + 1;
                    else dp2[i + 1] = 0;
                }
            }
        }
        //4. 返回值
        int ans = 0;
        for (int i = 0; i <= n; ++i)
            ans = max(max(ans, dp[i]), dp2[i]);
        return ans + 1;
    }
};

//139. 单词拆分

class Solution
{
public:
    bool wordBreak(string s, vector<string>& wordDict)
    {
        //优化
        unordered_set<string> hash;
        for (auto& s : wordDict) hash.insert(s);

        int n = s.size();
        vector<bool> dp(n + 1);
        dp[0] = true;// 保证后序填表是正确的
        s = " " + s;// 使原始字符串的下标统一加一
        for (int i = 1; i <= n; ++i)// 填dp[i]
        {
            for (int j = i; j >= 1; --j)// 最后一个单词的起始位置
            {
                if (dp[j - 1] && hash.count(s.substr(j, i - j + 1)))
                {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }
};