#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

//class Date
//{
//public:
//	//初始化列表
//	//初始化列表是每个成员变量定义初始化的位置
//	//能用初始化列表就建议用初始化列表
//	Date(int year, int month, int day, int& x)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//		, _n(1)
//		, ref(x)
//		, _p((int*)malloc(sizeof(int) * 4))
//	{
//		//赋值修改
//		if (_p == nullptr)
//		{
//			perror("malloc fail");
//			exit(-1);
//		}
//	}
//	void Print()
//	{
//		cout << _year << "年" << _month << "月" << _day << "日" << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//	int* _p;
//
//	//必须走初始化列表： const, &(引用), 没有默认构造的自定义类型成员
//	const int _n;
//	int& ref;
//};
//
//int main()
//{
//	int x = 10;
//	Date d1(2024, 1, 31, x);
//
//	d1.Print();
//	return 0;
//}

//统计构造次数 && static作用
//class A
//{
//public:
//	A(int a = 2)
//		: _a(a)
//	{
//		cout << "A(int a = 2)" << endl;
//		_count++;
//	}
//	//静态成员函数没有隐藏的this指针，不能访问任何非静态成员
//	//此函数用来输出private修饰的静态成员
//	static int GetCount()
//	{
//		//_a = 10;
//		return _count;
//	}
//		
//private:
//	int _a;
//	// 1. 静态成员为所有类对象所共享，不属于某个具体的对象，存放在静态区
//	// 2. 静态成员变量必须在类外定义，定义时不添加static关键字，类中只是声明
//	// 3. 类静态成员即可用 类名::静态成员 或者 对象.静态成员 来访问
//	// 4. 静态成员也是类的成员，受public、protected、private 访问限定符的限制
//	static int _count;
//};
//
//int A::_count = 0;
//
//int main()
//{
//	A(1);
//	A aa1 = 2;
//	A aa2 = aa1;
//	cout << aa1.GetCount() << endl;
//	cout << aa2.GetCount() << endl;
//	cout << A::GetCount() << endl;
//	return 0;
//}

//class A
//{
//public:
//	A(int a = 1)
//		:_a(a)
//	{
//		cout << "A()" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	//有名对象
//	A aa1;
//	
//
//	//匿名对象，作用域只在当前一行
//	A();
//	A(10);
//
//	A aa2(10);
//	return 0;
//}