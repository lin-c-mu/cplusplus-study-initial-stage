#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<string>
#include<unordered_map>
#include<vector>

using namespace std;

#define endl '\n'

//long long ret = 0;
//
//void Solution()
//{
//	//读入数据
//	int K;
//	cin >> K;
//	string s;
//	char char_l, char_r;
//	cin >> s >> char_l >> char_r;
//
//	//编写代码
//	int len = s.size();
//	for (int i = 0; i <= len - K; i++)
//	{
//		if (s[i] == char_l)
//		{
//			for (int j = i + K - 1; j < len; j++)
//			{
//				if (s[j] == char_r) ret++;
//			}
//		}
//	}
//}
//
//void Solution2()
//{
//	//读入数据
//	int K;
//	cin >> K;
//	string s;
//	char char_l, char_r;
//	cin >> s >> char_l >> char_r;
//
//	//
//	unordered_map<int, int> hash; //<下标， 个数>
//	int sum = 0;
//	for(int i = 0; i < s.size(); i++) 
//		if (s[i] == char_r)
//		{
//			sum++;
//			hash[i] = sum;
//		}
//	
//	for (int i = 0; i < s.size(); i++)
//	{
//		if (s[i] == char_l)
//		{
//			for (int j = i + K - 1; j < s.size(); j++)
//			{
//				if (s[j] == char_r)
//				{
//					ret += sum - hash[j] + 1;
//					break;
//				}
//			}
//		}
//	}
//}
//
//int main()
//{
//	std::ios::sync_with_stdio(0);
//	cin.tie(0);
//	cout.tie(0);
//
//
//
//	//Solution1();
//	Solution2();
//	cout << ret << endl;
//	return 0;
//}

//int m, n;
//vector<vector<bool>> vis;
//int ddx[8] = { 0, 0, 1, -1, 1, -1, 1, -1 };
//int ddy[8] = { 1, -1, 0, 0, 1, -1 ,-1, 1 };
//int dx[4] = { 1, -1, 0, 0 };
//int dy[4] = { 0, 0, 1, -1 };
//int ret;
//vector<vector<int>> st;
//
////找陆地
//void bbfs(int i, int j)
//{
//	vis[i][j] = true;
//	for (int k = 0; k < 4; k++)
//	{
//		int x = i + dx[k], y = j + dy[k];
//		if (x >= 0 && y >= 0 && x < m + 2 && y < n + 2 && !vis[x][y] && st[x][y] == 1)
//			bbfs(x, y);
//	}
//}
//
////找海洋
//void bfs(int i, int j)
//{
//	vis[i][j] = true;
//	for(int k = 0; k < 8; k++)
//	{
//		int x = i + ddx[k], y = j + ddy[k];
//		if (x >= 0 && y >= 0 && x < m + 2 && y < n + 2 && !vis[x][y])
//		{
//			if (st[x][y] == 1)
//			{
//				bbfs(x, y);
//				ret++;
//			}
//			else
//				bfs(x, y);
//		}
//	}
//}
//
//void Solution()
//{
//	ret = 0;
//	cin >> m >> n;
//	vis = vector<vector<bool>>(m + 2, vector<bool>(n + 2));
//	st = vector<vector<int>>(m + 2, vector<int>(n + 2));
//	//1.读入岛屿数据
//	string s;
//	for (int i = 1; i <= m; i++)
//	{
//		cin >> s;
//		for (int j = 1; j <= n; j++)
//		{
//			st[i][j] = s[j - 1] - '0';
//		}
//	}
//	//2.找岛屿
//	bfs(0, 0);
//}
//
//int main()
//{
//	std::ios::sync_with_stdio(0);
//	cin.tie(0);
//	cout.tie(0);
//
//	int count;
//	cin >> count;
//	while (count--)
//	{
//		Solution();
//		cout << ret << endl;
//	}
//
//	return 0;
//}


int main()
{
	cout << __cplusplus << endl;
	return 0;
}