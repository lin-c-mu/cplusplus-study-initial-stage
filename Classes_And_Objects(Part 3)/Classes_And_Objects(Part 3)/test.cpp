#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

//1. const成员

//class Date
//{
//public:
//	//void Print(const Date& this);
//	void Print() const;
//
//
//	Date* operator&()
//	{
//		return this;
//	}
//	const Date* operator&() const
//	{
//		return this;
//	}
//private:
//	int _year; // 年
//	int _month; // 月
//	int _day; // 日
//};

//2. 构造函数 -- 初始化列表

////以下三类需要使用初始化列表初始化：引用，const，没有默认构造函数
// //先走初始化列表
//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B(int a, int ref)
//		:_aobj(a)
//		, _ref(ref)
//		, _n(10)
//	{}
//private:
//	A _aobj;  // 没有默认构造函数
//	int& _ref;  // 引用
//	const int _n; // const 
//};

//class Time
//{
//public:
//    Time(int hour = 0)
//        :_hour(hour)
//    {
//        std::cout << "Time()" << std::endl;
//    }
//private:
//        int _hour;
//};
//class Date
//{
//public:
//    Date(int day)
//    {}
//private:
//    int _day;
//    Time _t;
//};
//int main()
//{
//    Date d(1);
//}
//}

////声明顺序决定初始化列表初始化的顺序
//class A
//{
//public:
//    A(int a)
//        :_a1(a)
//        , _a2(_a1)
//    {}
//
//    void Print() {
//        std::cout << _a1 << " " << _a2 << std::endl;
//    }
//private:
//    int _a2;
//    int _a1;
//};
//int main() 
//{
//    A aa(1);
//    aa.Print();
//}


//2. 构造函数 -- explicit关键字

////explicit
//class Date
//{
//public:
//	// 1. 单参构造函数，没有使用explicit修饰，具有类型转换作用
//	 // explicit修饰构造函数，禁止类型转换---explicit去掉之后，代码可以通过编译
//	explicit Date(int year)
//		:_year(year)
//	{}
//	/*
//	// 2. 虽然有多个参数，但是创建对象时后两个参数可以不传递，没有使用explicit修饰，具
//   有类型转换作用
//	// explicit修饰构造函数，禁止类型转换
//	explicit Date(int year, int month = 1, int day = 1)
//	: _year(year)
//	, _month(month)
//	, _day(day)
//	{}
//	*/
//	Date& operator=(const Date& d)
//	{
//		if (this != &d)
//		{
//			_year = d._year;
//			_month = d._month;
//			_day = d._day;
//		}
//		return *this;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//void Test()
//{
//	Date d1(2022);
//	// 用一个整形变量给日期类型对象赋值
//	// 实际编译器背后会用2023构造一个无名对象，最后用无名对象给d1对象进行赋值
//	//d1 = 2023;
//	// 将1屏蔽掉，2放开时则编译失败，因为explicit修饰构造函数，禁止了单参构造函数类型转换的作用
//}

//class C
//{
//public:
//	C(int c = 0)
//		:_c(c)
//	{}
//private:
//	int _c;
//};
//
//int main()
//{
//	C cc1(10);
//	C cc2 = 2;
//	return 0;
//}

////隐式类型转换
//class MyStack
//{
//public:
//	void Push(const C& c) { /* ... */ }
//private:
//	C cc;
//};
//int main()
//{
//	MyStack st1;
//	C cc1(1);
//	st1.Push(cc1);
//
//	st1.Push(2);
//	return 0;
//}


//3. static

////实现一个类，计算程序中创建出了多少个类对象。
//class A 
//{
//public:
//	A() { ++_scount; }
//	A(const A& t) { ++_scount; }
//	~A() { --_scount; }
//	static int GetACount() { return _scount; }
//private:
//	static int _scount;
//};
//int A::_scount = 0;
//void TestA()
//{
//	std::cout << A::GetACount() << std::endl;
//	A a1, a2;
//	A a3(a1);
//	std::cout << A::GetACount() << std::endl;
//}
//int main()
//{
//	TestA();
//	return 0;
//}

//4. 友元

//class Time
//{
//	friend class Date;  //声明日期类为时间类的友元类，则在日期类中直接访问Time类中的私有成员变量
//public:
//	Time(int hour = 0, int minute = 0, int second = 0)
//		:_hour(hour)
//		,_minute(minute)
//		,_second(second)
//	{}
//
//	//需要声明和定义分离
//	void Func(const Date& d)
//	{
//		//d._day;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//	friend class Time;
//public:
//	Date(int year = 2024, int month = 6, int day = 26)
//		:_year(year)
//		,_month(month)
//		,_day(day)
//	{}
//	void TimePrint()
//	{
//		//直接访问时间类私有的成员变量
//		std::cout << _t._hour << ":" << _t._minute << ":" << _t._second << std::endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//
//	Time _t;
//};
//
//int main()
//{
//	Date d1;
//	return 0;
//}

//5. 内部类

//class A
//{
//public:
//	//B类受A类的类域限制
//	class B
//	{
//	public:
//		void func(const A& a)
//		{
//			//B 天生就是 A 的友元
//			std::cout << a._a << _c <<  std::endl;
//		}
//	private:
//		int _b;
//	};
//
//	void fx(const B& b)
//	{
//		//std::cout << b._b << std::endl;  //不可访问，单向友元
//	}
//private:
//	int _a;
//	static int _c;
//};
//int _c = 10;
//
//int main()
//{
//	A::B b;   //若 B 设为private，则不可访问
//	std::cout << sizeof(A) << std::endl;  // ->  4
//	return 0;
//}

//6. 匿名对象

//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		std::cout << "A(int a)" << std::endl;
//
//}
//	~A()
//	{
//		std::cout << "~A()" << std::endl;
//	}
//private:
//	int _a;
//};
//class Solution {
//public:
//	int Sum_Solution(int n) {
//		//...
//		return n;
//	}
//};
//int main()
//{
//	//有名对象
//	A aa1;
//	//匿名对象
//	A();
//	A aa2(2);
//
//	Solution().Sum_Solution(10);
//	return 0;
//}

