#define  _CRT_SECURE_NO_WARNINGS 1

//2244. 完成所有任务需要的最少轮数

class Solution {
public:
    int minimumRounds(vector<int>& tasks) {
        unordered_map<int, int> cnt;
        for (int t : tasks) {
            cnt[t]++;
        }
        int res = 0;
        for (auto [_, v] : cnt) {
            if (v == 1) {
                return -1;
            }
            else if (v % 3 == 0) {
                res += v / 3;
            }
            else {
                res += v / 3 + 1;
            }
        }
        return res;
    }
};