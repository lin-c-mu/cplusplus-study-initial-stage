#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

void test_string1()
{
	string filename("test.cpp");
	FILE* fout = fopen(filename.c_str(), "r");


	//拿文件后缀
	//s.rfind(xxx, size_t pos = 0) -- 从后往前找;   string s.substr(xxx, size_t string::npos) -- 拷贝string
	string s1("test.cpp.zip");
	string suffix;
	size_t pos = s1.rfind('.');
	if (pos != string::npos)
	{
		//suffix = s1.substr(pos);
		suffix = s1.substr(pos, s1.size() - pos);
		cout << suffix << endl;
	}
	else
	{
		cout << "没有后缀" << endl;
	}
	cout << "-------------------------" << endl;


	string url("https://legacy.cplusplus.com/reference/string/string/find/");

	string protocol, domain, uri;
	size_t pos1 = url.find(':');
	if (pos1 != string::npos)
	{
		protocol = url.substr(0, pos1);
		cout << "protocol: " << protocol << endl;
	}
	size_t pos2 = url.find('/', pos + 3);
	if (pos2 != string::npos)
	{
		domain = url.substr(pos1 + 3, pos2 - pos1 - 3);
		cout << "domain: " << domain << endl;
	}
	uri = url.substr(pos2 + 1, url.size() - pos2 - 1);
	//uri = url.substr(pos2 + 1);
	cout << "uri: " << uri << endl;
}

void test_string2()
{
	string str("Please, replace the vowels in this sentence by asterisks.");
	size_t found = str.find_first_not_of("aeiou");
	while (found != std::string::npos)
	{
		str[found] = '*';
		found = str.find_first_not_of("aeiou", found + 1);
	}

	std::cout << str << '\n';
}


void test_string3()
{
	string str;
	cout << "This is mine";
	//getline(cin str);
}

int main()
{
	//test_string1();
	//test_string2();
	test_string3();

	return 0;
}