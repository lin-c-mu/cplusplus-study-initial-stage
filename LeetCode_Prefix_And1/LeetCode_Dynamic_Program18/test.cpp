#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
#include<vector>

//DP34 【模板】前缀和

int main()
{
    int len, count;
    cin >> len >> count;
    //1.将数据读入数组
    vector<int> arr(len + 1);
    for (int i = 1; i <= len; i++) cin >> arr[i];
    //2.实现前缀和数组 [1,i]
    vector<long long> dp(len + 1); //防止溢出
    for (int i = 1; i <= len; i++) dp[i] = dp[i - 1] + arr[i];
    //3.根据dp表，求值
    int left = 0, right = 0;
    for (int i = 0; i < count; i++)
    {
        cin >> left >> right;
        cout << dp[right] - dp[left - 1] << endl;
    }
    return 0;
}

//DP35 【模板】二维前缀和

int main()
{
    int col, row, count;
    cin >> col >> row >> count;
    vector<vector<int>> arr(col + 1, vector<int>(row + 1));
    //1.填入数据
    for (int i = 1; i <= col; i++)
        for (int j = 1; j <= row; j++)
            cin >> arr[i][j];
    //2.实现前缀和 [l1, r1] - [l2, r2]
    vector<vector<long long>> dp(col + 1, vector<long long>(row + 1));
    for (int i = 1; i <= col; i++)
        for (int j = 1; j <= row; j++)
            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + arr[i][j];
    //3.使用前缀和表求值
    int l1, r1, l2, r2;
    for (int i = 1; i <= count; i++)
    {
        cin >> l1 >> r1 >> l2 >> r2;
        cout << dp[l2][r2] - dp[l1 - 1][r2] - dp[l2][r1 - 1] + dp[l1 - 1][r1 - 1] << endl;
    }
    return 0;
}
