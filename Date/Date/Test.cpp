#define  _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

void test1()
{
	Date d1(2024, 1, 30);
	Date d2(d1);
	Date d3 = d1;

	d3 = d1 + 10000;
	d1.Print();
	d3.Print();

	d1 = d1 += 30;
	d1.Print();

	//�ȼ��ڣ�  d2 = operator - (Date& d2, int day);
	d2 = d2 - 10000;
	d2.Print();
	Date d4(d2);
	d2 = d2 -= 10000;
	Date d5(d2);
	d2.Print();

	putchar('\n');
	int ret = d5 - d4;
	d5.Print();
	d4.Print();
	printf("%d\n", ret);

	Date d6 = d4++;
	d4.Print();
	d6.Print();
	d4.operator++(10);
	d4.Print();

	d4.operator++();
	d4.Print();
	cout << d4;
}

void test2()
{
	Date d1(2024, 2, 1);
	cout << d1;
	Date d2 = d1;
	cin >> d2;
	cout << d2;
}

int main()
{
	//test1();
	//test2();
	Date d3(2024, 2, 2);
	Date d4(2024, 3, 5);
	int ret = d3.GetMonthDay(d4);
	printf("%d\n", ret);
	return 0;
}