#define  _CRT_SECURE_NO_WARNINGS 1

#pragma once

#include<iostream>
#include<stdbool.h>

using namespace std;

class Date
{
public:
	//构造函数
	Date(int year = 2024, int month = 1, int day = 30)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//析构，赋值，

	//比较
	bool operator <= (const Date& d) const;
	bool operator < (const Date& d) const;
	bool operator >= (const Date& d) const;
	bool operator > (const Date& d) const;
	bool operator == (const Date& d) const;
	bool operator != (const Date& d) const;

	//增减
	Date operator + (int day) const;
	Date& operator += (int day);
	Date operator - (int day) const;
	Date& operator -= (int day);

	int operator - (const Date& d) const;

	//自增，自减
	// 特殊处理：解决语法逻辑不自洽，自相矛盾的问题
	// d1++
	// 为了跟前置++区分，强行增加一个int形参，够成重载区分
	Date& operator ++ ();
	Date operator ++ (int);
	Date& operator -- ();
	Date operator -- (int);


	int GetMonthDay(const Date& d) const
	{
		int day[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (d._month == 2 && ((d._year % 4 == 0 && d._year % 100 != 0) || d._year % 400 == 0))
			return 29;
		else
			return day[d._month];
	}

	bool DayInvalid() const
	{
		if (_year < 0
			|| _month < 1 || _month > 12
			|| _day < 1 || _day > GetMonthDay(*this))
			return false;
		else
			return true;
	}

	void Print() const
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

	friend ostream& operator << (ostream& out, Date& d);
	friend istream& operator >> (istream& in, Date& d);

private:
	int _year;
	int _month;
	int _day;
};

//流插入运算符
ostream& operator << (ostream& out, Date& d);
istream& operator >> (istream& in, Date& d);
