#define  _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

//比较
bool Date::operator <= (const Date& d) const
{
	return *this < d || *this == d;
}

bool Date::operator < (const Date& d) const
{
	if (_year == d._year)
	{
		if (_month == d._month)
		{
			if (_day < d._day)
				return true;
		}
		else if (_month < d._month)
		{
			return true;
		}
	}
	else if (_year < d._year)
	{
		return true;
	}
	return false;
}

bool Date::operator != (const Date& d) const
{
	return !(*this == d);
}

bool Date::operator >= (const Date& d) const
{
	return !(*this < d);
}

bool Date::operator > (const Date& d) const
{
	return !(*this <= d);
}

bool Date::operator == (const Date& d) const
{
	if (_year == d._year && _month == d._month && _day == d._day)
		return true;
	return false;
}

//增减
Date Date::operator + (int day) const
{
	Date d = *this;
	d += day;
	return d;
}

Date& Date::operator += (int day)
{
	_day += day;
	while (_day > GetMonthDay(*this))
	{
		_day -= GetMonthDay(*this);
		_month++;
		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}

//方法二：
// 较方法一，空间内耗较大，性能劣于方法一
//Date Date::operator+(int day)
//{
//	Date d = *this;
//	d._day += day;
//	while (d._day > GetMonthDay(d))
//	{
//		d._day -= GetMonthDay(d);
//		d._month++;
//		if (d._month == 13)
//		{
//			d._year++;
//			d._month = 1;
//		}
//	}
//	return d;
//}
//Date& Date::operator+=(int day)
//{
//	*this = *this + day;
//	return *this;
//}

Date Date::operator - (int day) const
{
	Date d = *this;
	d = d -= day;
	return d;
}

Date& Date::operator -= (int day)
{
	_day -= day;
	while (_day < 1)
	{
		if (_month == 1)
		{
			_year--;
			_month = 12;
		}
		else
		{
			_month--;
		}
		_day = _day + GetMonthDay(*this);

	}
	return *this;
}



int Date::operator - (const Date& d) const
{
	int flag = 1;
	Date max = *this;
	Date min = d;
	if (min > max)
	{
		flag = -1;
		max = d;
		min = *this;
	}
	int n = 0;
	while (min != max)
	{
		n++;
		++min;
	}
	return n;
}

//劣  有问题
//int Date::operator - (const Date& d)
//{
//	int flag = 1;
//	Date max = *this;
//	Date min = d;
//	if (min > max)
//	{
//		flag = -1;
//		max = d;
//		min = *this;
//	}
//	int year_day = 0;
//	while(min._year != max._year)
//	{
//		year_day += 365;
//		if ((min._year % 4 == 0 && min._year % 100 == 0) || min._year % 400 == 0)
//			year_day += 1;
//		min._year++;
//	}
//	if (max._month == min._month)
//	{
//		return (max._day - min._day + year_day) * flag;
//	}
//	else
//	{
//		int day = 0;
//		while (min._month != max._month)
//		{
//			min._month++;
//			day += GetMonthDay(min);
//		}
//		return (day + max._day - min._day + year_day) * flag;
//	}
//}

//自增，自减
//前置++

Date& Date::operator ++ ()
{
	*this += 1;
	return *this;
}

//Date& Date::operator ++ ()
//{
//	if (_day == GetMonthDay(*this))
//	{
//		_day = 1;
//		_month++;
//		if (_month == 13)
//		{
//			_year++;
//			_month = 1;
//		}
//	}
//	else 
//	{
//		_day++;
//	}
//	return *this;
//}

//后置++
Date Date::operator ++ (int)
{
	Date d = *this;
	*this += 1;
	return d;
}

Date& Date::operator -- ()
{
	*this -= 1;
	return *this;
}

Date Date::operator -- (int)
{
	Date d = *this;
	*this -= 1;
	return d;
}

ostream& operator << (ostream& out, Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	return out;
}

istream& operator >> (istream& in, Date& d)
{
	while (1)
	{
		cout << "请输入年/月/日：" << endl;
		in >> d._year >> d._month >> d._day;
		if (!(d.DayInvalid()))
			cout << "输入有误，请重新输入 :>" << endl;
		else
			break;
	}
	return in;
}