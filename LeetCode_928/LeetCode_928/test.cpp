#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//928. 尽量减少恶意软件的传播 II

//dfs

class Solution {
public:
    int minMalwareSpread(vector<vector<int>>& graph, vector<int>& initial) {
        int n = graph.size();
        vector<int> initialSet(n);
        for (int v : initial) {
            initialSet[v] = 1;
        }
        vector<vector<int>> infectedBy(n);
        for (int v : initial) {
            vector<int> infectedSet(n);
            dfs(graph, initialSet, infectedSet, v);
            for (int u = 0; u < n; u++) {
                if (infectedSet[u] == 1) {
                    infectedBy[u].push_back(v);
                }
            }
        }
        vector<int> count(n);
        for (int u = 0; u < n; u++) {
            if (infectedBy[u].size() == 1) {
                count[infectedBy[u][0]]++;
            }
        }
        int res = initial[0];
        for (int v : initial) {
            if (count[v] > count[res] || count[v] == count[res] && v < res) {
                res = v;
            }
        }
        return res;
    }

    void dfs(vector<vector<int>>& graph, vector<int>& initialSet, vector<int>& infectedSet, int v) {
        int n = graph.size();
        for (int u = 0; u < n; u++) {
            if (graph[v][u] == 0 || initialSet[u] == 1 || infectedSet[u] == 1) {
                continue;
            }
            infectedSet[u] = 1;
            dfs(graph, initialSet, infectedSet, u);
        }
    }
};
