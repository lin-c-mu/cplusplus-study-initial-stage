#define  _CRT_SECURE_NO_WARNINGS 1

//2644. 找出可整除性得分最大的整数

class Solution {
public:
    int maxDivScore(vector<int>& nums, vector<int>& divisors) {
        int cnt = -1, ans = 0;

        for (int i = 0; i < divisors.size(); i++) {
            int tmp = 0;
            for (int j = 0; j < nums.size(); j++) {
                if (nums[j] % divisors[i] == 0) {
                    tmp++;
                }
            }

            if (tmp > cnt || (tmp == cnt && divisors[i] < ans)) {
                ans = divisors[i];
                cnt = tmp;
            }
        }
        return ans;
    }
};