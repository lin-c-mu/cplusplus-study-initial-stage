#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//123. 买卖股票的最佳时机 III

class Solution
{
public:
    int maxProfit(vector<int>& prices)
    {
        int n = prices.size();
        //1. 创建dp表 -- f->买入; g->卖出
        vector<vector<int>> f(n, vector<int>(3, -0x3f3f3f3f));
        auto g = f;
        //2. 初始化
        f[0][0] = -prices[0], g[0][0] = 0;
        //3. 填表
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < 3; ++j)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j - 1 >= 0) g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }
        }
        //4. 返回值
        return max(max(g[n - 1][0], g[n - 1][1]), g[n - 1][2]);
    }
};

//188. 买卖股票的最佳时机 IV

class Solution
{
public:
    int maxProfit(int k, vector<int>& prices)
    {
        int n = prices.size();
        k = min(k, n / 2);
        //1. 创建dp表 -- f->买入; g->卖出
        vector<vector<int>> f(n, vector<int>(k + 1, -0x3f3f3f3f));
        auto g = f;
        //2. 初始化
        f[0][0] = -prices[0], g[0][0] = 0;
        //3. 填表
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j <= k; ++j)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j - 1 >= 0) g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }
        }
        //4. 返回值
        int ans = 0;
        for (int i = 0; i <= k; ++i)
            ans = ans > g[n - 1][i] ? ans : g[n - 1][i];
        return ans;
    }
};