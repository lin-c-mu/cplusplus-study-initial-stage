#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//200. 岛屿数量

class Solution
{
    int dx[4] = { 0, 0, -1, 1 };
    int dy[4] = { 1, -1, 0, 0 };
    int m, n;
    vector<vector<bool>> vis;

public:
    int numIslands(vector<vector<char>>& grid)
    {
        int ret = 0;
        m = grid.size(), n = grid[0].size();
        vis = vector<vector<bool>>(m, vector<bool>(n));
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] == '1' && !vis[i][j])
                {
                    ret++;
                    dfs(i, j, grid);
                }
            }
        }
        return ret;
    }
    void dfs(int i, int j, vector<vector<char>>& grid)
    {
        vis[i][j] = true;
        for (int k = 0; k < 4; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && y >= 0 && x < m && y < n && !vis[x][y] && grid[x][y] == '1')
                dfs(x, y, grid);
        }
    }
};

//695. 岛屿的最大面积

class Solution
{
    int dx[4] = { 0, 0, 1, -1 };
    int dy[4] = { 1, -1, 0, 0 };
    int m, n;
    int prev;
    vector<vector<bool>> vis;
public:
    int maxAreaOfIsland(vector<vector<int>>& grid)
    {
        int ret = 0;
        m = grid.size(), n = grid[0].size();
        vis = vector<vector<bool>>(m, vector<bool>(n));
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
            {
                if (!vis[i][j] && grid[i][j] == 1)
                {
                    prev = 1;
                    dfs(i, j, grid);
                    ret = max(ret, prev);
                }
            }
        return ret;
    }
    void dfs(int i, int j, vector<vector<int>>& grid)
    {
        vis[i][j] = true;
        for (int k = 0; k < 4; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && y >= 0 && x < m && y < n && !vis[x][y] && grid[x][y] == 1)
            {
                prev++;
                dfs(x, y, grid);
            }
        }
    }
};

//130. 被围绕的区域

class Solution
{
    int dx[4] = { 0, 0, 1, -1 };
    int dy[4] = { 1, -1, 0, 0 };
    int m, n;
public:
    void solve(vector<vector<char>>& board)
    {
        m = board.size(), n = board[0].size();
        for (int i = 0; i < n; i++)
        {
            if (board[0][i] == 'O')
                dfs(board, 0, i);
            if (board[m - 1][i] == 'O')
                dfs(board, m - 1, i);
        }
        for (int i = 0; i < m; i++)
        {
            if (board[i][0] == 'O')
                dfs(board, i, 0);
            if (board[i][n - 1] == 'O')
                dfs(board, i, n - 1);
        }
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (board[i][j] == '.') board[i][j] = 'O';
                else if (board[i][j] == 'O') board[i][j] = 'X';
            }
        }
    }
    void dfs(vector<vector<char>>& board, int i, int j)
    {
        board[i][j] = '.';
        for (int k = 0; k < 4; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && y >= 0 && x < m && y < n && board[x][y] == 'O')
                dfs(board, x, y);
        }
    }
};

//417. 太平洋大西洋水流问题

class Solution
{
    int dx[4] = { 0, 0, 1, -1 };
    int dy[4] = { 1, -1, 0, 0 };
    int m, n;
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights)
    {
        m = heights.size(), n = heights[0].size();
        vector<vector<bool>> pac(m, vector<bool>(n));
        vector<vector<bool>> atl = pac;
        vector<vector<int>> ret;

        for (int i = 0; i < n; i++)
        {
            dfs(0, i, heights, pac);
            dfs(m - 1, i, heights, atl);
        }
        for (int j = 0; j < m; j++)
        {
            dfs(j, 0, heights, pac);
            dfs(j, n - 1, heights, atl);
        }
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (pac[i][j] && atl[i][j]) ret.push_back({ i, j });
            }
        }
        return ret;
    }
    void dfs(int i, int j, vector<vector<int>>& heights, vector<vector<bool>>& judge)
    {
        judge[i][j] = true;
        for (int k = 0; k < 4; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && y >= 0 && x < m && y < n && heights[x][y] >= heights[i][j] && !judge[x][y])
                dfs(x, y, heights, judge);
        }
    }
};

//529. 扫雷游戏

class Solution
{
    int dx[8] = { 0, 0, 1, -1, 1, 1, -1, -1 };
    int dy[8] = { 1, -1, 0, 0, 1, -1, 1, -1 };
    int m, n;
public:
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click)
    {
        m = board.size(), n = board[0].size();
        if (board[click[0]][click[1]] == 'M')
        {
            board[click[0]][click[1]] = 'X';
            return board;
        }
        dfs(click[0], click[1], board);
        return board;
    }
    void dfs(int i, int j, vector<vector<char>>& board)
    {
        int count = 0;
        for (int k = 0; k < 8; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && y >= 0 && x < m && y < n && board[x][y] == 'M') count++;
        }
        if (count == 0)
            for (int k = 0; k < 8; k++)
            {
                int x = i + dx[k], y = j + dy[k];
                board[i][j] = 'B';
                if (x >= 0 && y >= 0 && x < m && y < n && board[x][y] == 'E') dfs(x, y, board);
            }
        else
            board[i][j] = count + '0';
    }
};