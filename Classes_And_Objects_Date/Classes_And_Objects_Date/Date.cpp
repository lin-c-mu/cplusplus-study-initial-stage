#define  _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

bool Date::operator==(const Date& d) const
{
	return _year == d._year 
		&& _month == d._month
		&& _day == d._day;
}

bool Date::operator>=(const Date& d) const
{
	return *this > d || *this == d;
}

bool Date::operator<=(const Date& d) const
{
	return !(*this > d);
}

bool Date::operator>(const Date& d) const
{
	if (_year > d._year)
		return true;
	else if (_year == d._year)
	{
		if (_month > d._month)
			return true;
		else if (_month == d._month)
		{
			if (_day > d._day)
				return true;
		}
	}
	return false;
}

bool Date::operator<(const Date& d) const
{
	return !(*this >= d);
}

bool Date::operator!=(const Date& d) const
{
	return !(*this == d);
}

Date& Date::operator+=(int day)
{
	_day += day;
	while (_day > GetMonthDay())
	{
		_day -= GetMonthDay();
		_month++;
		if (_month > 12)
		{
			_month = 1;
			_year++;
		}
	}
	return *this;
}

Date Date::operator+(int day) const
{
	Date tmp(*this);
	tmp += day;
	return tmp;
}

Date& Date::operator-=(int day)
{
	_day -= day;
	while (_day < 1)
	{
		_month--;
		if (_month < 1)
		{
			_month = 12;
			_year--;
		}
		_day += GetMonthDay();
	}
	return *this;
}

Date Date::operator-(int day) const
{
	Date tmp(*this);
	tmp -= day;
	return tmp;
}

int Date::operator-(const Date& d) const
{
	Date max = *this;
	Date min = d;
	if (max < min)
	{
		max = d;
		min = *this;
	}
	int count = 0;
	while (min != max)
	{
		count++;
		min += 1;
	}
	return count;
}

Date& Date::operator++()   //前置
{
	return *this += 1;
}

Date Date::operator++(int) //后置
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}

Date& Date::operator--()   //前置
{
	return *this -= 1;
}

Date Date::operator--(int) //后置
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}

std::ostream& operator<<(std::ostream& out, const Date& d)
{
	out << d._year << "-" << d._month << "-" << d._day << std::endl;
	return out;
}

std::istream& operator>>(std::istream& in, Date& d)
{
	while(1)
	{
		std::cout << "请输入年月日:>";
		in >> d._year >> d._month >> d._day;
		if (!d.CheckInvalid())
			std::cout << "亲，您的输入非法，请重新输入: " << std::endl;
		else 
			break;
	}
	return in;
}