#define  _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

int main()
{
	Date d1(2024, 6, 26);
	Date d2 = d1;
	std::cout << (d1 == d2) << std::endl;
	d1 += 1000;
	d1.Print();
	std::cout << d1 - d2 << std::endl;
	Date d3 = d2 + 2000;
	d3.Print();
	
	d3 = d2 = d1;

	std::cout << d2;


	//Date d4;
	//std::cin >> d4;

	Date d5(2025, 12, 35);
	d5.Print();
	return 0;
}