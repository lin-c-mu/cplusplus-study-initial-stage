#pragma once

#include<iostream>
#include<assert.h>

class Date
{
public:
	Date(int year = 2024, int month = 6, int day = 25)
	{
		assert(month >= 1 || month <= 12);
		_year = year;
		_month = month;
		_day = day;
	}

	int GetMonthDay() const
	{
		static int MonthOfDay[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (_month == 2 && ((_year % 4 == 0 && _year % 100 != 0) || _year % 400 == 0))
			return 29;
		return MonthOfDay[_month];
	}

	void Print()
	{
		if (!CheckInvalid()) std::cout << "日期非法... " << this << ": ";
		std::cout << _year << "-" << _month << "-" << _day << std::endl;
	}

	bool CheckInvalid() const
	{
		if (_year <= 0
			|| _month > 12 || _month < 1
			|| _day < 1 || _day > GetMonthDay())
			return false;
		else
			return true;
	}

	bool operator==(const Date& d) const;
	bool operator>=(const Date& d) const;
	bool operator<=(const Date& d) const;
	bool operator>(const Date& d) const;
	bool operator<(const Date& d) const;
	bool operator!=(const Date& d) const;

	Date& operator+=(int day);
	Date operator+(int day) const;
	Date& operator-=(int day);
	Date operator-(int day) const;
	int operator-(const Date& d) const;

	Date& operator++();   //前置
	// 为了跟前置++区分，强行增加一个int形参，够成重载区分
	Date operator++(int); //后置
	Date& operator--();   //前置
	Date operator--(int); //后置

	friend std::ostream& operator<<(std::ostream& out, const Date& d);
	friend std::istream& operator>>(std::istream& in, Date& d);
private:
	int _year;
	int _month;
	int _day;
};

std::ostream& operator<<(std::ostream& out, const Date& d);
std::istream& operator>>(std::istream& in, Date& d);