#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//931. 下降路径最小和

class Solution
{
public:

    //    int findMin(int a, int b, int c)
    //    {
    //        return mi
    //    }
    int minFallingPathSum(vector<vector<int>>& matrix)
    {
        //1. 创建dp表
        int row = matrix.size(), col = matrix.size();
        vector<vector<int>> dp(row + 1, vector<int>(col + 2, INT_MAX));
        //2. 初始化列表
        //第一行变为0
        for (int i = 0; i < col + 2; ++i) dp[0][i] = 0;
        //3. 填表
        for (int i = 1; i <= row; ++i)
            for (int j = 1; j <= col; ++j)
                dp[i][j] = matrix[i - 1][j - 1]
                + min(min(dp[i - 1][j - 1], dp[i - 1][j]), dp[i - 1][j + 1]);
        //4. 返回结果
        int ans = INT_MAX;
        for (int i = 1; i <= col; ++i)
            ans = min(ans, dp[row][i]);
        return ans;
    }
};

//64. 最小路径和

class Solution
{
public:
    int minPathSum(vector<vector<int>>& grid)
    {
        int m = grid.size(), n = grid[0].size();
        //1. 创建dp表
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
        //2. 初始化
        dp[0][1] = 0;
        //3. 填表
        for (int i = 1; i <= m; ++i)
            for (int j = 1; j <= n; ++j)
                dp[i][j] = grid[i - 1][j - 1] + min(dp[i][j - 1], dp[i - 1][j]);
        //返回结果
        return dp[m][n];
    }
};

//174. 地下城游戏

//公主找骑士版
class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        int n = dungeon.size(), m = dungeon[0].size();
        vector<vector<int>> dp(n + 1, vector<int>(m + 1, INT_MAX));
        dp[n][m - 1] = dp[n - 1][m] = 1;
        for (int i = n - 1; i >= 0; --i) {
            for (int j = m - 1; j >= 0; --j) {
                int minn = min(dp[i + 1][j], dp[i][j + 1]);
                dp[i][j] = max(minn - dungeon[i][j], 1);
            }
        }
        return dp[0][0];
    }
};

//半成品 -- 骑士救公主版（有小问题）
class Solution
{
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon)
    {
        int m = dungeon.size(), n = dungeon[0].size();
        //1. 创建 dp 表
        vector<vector<int>> dp1(m + 1, vector<int>(n + 1, INT_MIN));
        vector<vector<int>> dp2(m + 1, vector<int>(n + 1, INT_MIN));
        //2。 初始化
        dp1[1][0] = dp2[1][0] = 0;
        //3. 填表
        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                int king = 0, left = 0;
                if (dp1[i - 1][j] > dp1[i][j - 1]) king = 1;
                else left = 1;

                dp1[i][j] = max(dp1[i - 1][j], dp1[i][j - 1]);

                if (king == 1)
                    dp2[i][j] = dungeon[i - 1][j - 1] + dp2[i - 1][j];
                else
                    dp2[i][j] = dungeon[i - 1][j - 1] + dp2[i][j - 1];

                if (dp2[i][j] < dp1[i][j]) dp1[i][j] = dp2[i][j];
            }
        }
        //4. 返回值
        return -dp1[m][n] + 1;
    }
};