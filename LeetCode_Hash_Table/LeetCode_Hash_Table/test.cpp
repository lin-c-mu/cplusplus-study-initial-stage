#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>
#include<unordered_map>
#include<string.h>

//1. 两数之和

class Solution
{
public:
    vector<int> twoSum(vector<int>& nums, int target)
    {
        unordered_map<int, int> hash;
        for (int i = 0; i < nums.size(); i++)
        {
            int x = target - nums[i];
            if (hash.count(x)) return { hash[x], i };
            hash[nums[i]] = i;
        }
        return { -1, -1 };
    }
};

//面试题 01.02. 判定是否互为字符重排

class Solution
{
public:
    bool CheckPermutation(string s1, string s2)
    {
        if (s1.size() != s2.size()) return false;
        unordered_map<char, int> hash;
        for (auto ch : s1) hash[ch]++;
        for (auto ch : s2)
        {
            if (hash[ch] == 0) return false;
            else hash[ch]--;
        }

        return true;
    }
};

//217. 存在重复元素

class Solution
{
public:
    bool containsDuplicate(vector<int>& nums)
    {
        unordered_map<int, int> hash;
        for (auto n : nums)
            if (++hash[n] > 1) return true;
        return false;
    }
};

//219. 存在重复元素 II

class Solution
{
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k)
    {
        unordered_map<int, int> hash;
        for (int i = 0; i < nums.size(); i++)
        {
            if (hash[nums[i]] && abs(i - hash[nums[i]] + 1) <= k) return true;
            hash[nums[i]] = i + 1;
        }
        return false;
    }
};

//49. 字母异位词分组

class Solution
{
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs)
    {
        unordered_map<string, vector<string>> hash;

        //1. 把所有的字母异位词分组
        for (auto& s : strs)
        {
            string tmp = s;
            sort(tmp.begin(), tmp.end());
            hash[tmp].push_back(s);
        }

        //2. 结果提取出来
        vector<vector<string>> ret;
        for (auto& [x, y] : hash)
            ret.push_back(y);
        return ret;
    }
};