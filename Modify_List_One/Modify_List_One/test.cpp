#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

namespace lzw
{
	template<class T>
	struct ListNode
	{
		ListNode<T>* _prev;
		ListNode<T>* _next;
		T _val;
	};

	template<class T>
	struct ListIterator
	{
		typedef ListNode<T> Node;
		typedef ListIterator<T> Self;

		ListIterator(Node* node)
			:_node(node)
		{}

		T& operator*()
		{
			return _node->_val;
		}

		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}

		Self& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		Self operator++(int)
		{
			Self it = *this;
			_node = _node->_next;
			return it;
		}

		Node* _node;

	};

	template<class T>
	class list
	{
	public:
		typedef ListNode<T> Node;
		typedef ListIterator<T> iterator;
		
		iterator begin()
		{
			//缺省变量  -- 等价于
			//iterator it(_node->_next);
			//return it;
			return iterator(_node->_next);
		}

		iterator end()
		{
			//缺省变量
			return iterator(_node);
		}

		list()
		{
			_node = new Node;
			_node->_prev = _node;
			_node->_next = _node;
			_node->_val = -1;
		}

		void push_back(const T& x)
		{
			Node* tail = _node->_prev;

			Node* newnode = new Node;
			tail->_next = newnode;
			newnode->_prev = tail;
			newnode->_next = _node;
			_node->_prev = newnode;
			newnode->_val = x;
		}

	private:
		Node* _node;
	};

	void test_list1()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);

		list<int>::iterator it = lt.begin();
		while (it != lt.end())
		{
			cout << *it << "<->";
			it++;
		}
		cout << "-1" << endl;
	}

}

int main()
{
	lzw::test_list1();
	return 0;
}