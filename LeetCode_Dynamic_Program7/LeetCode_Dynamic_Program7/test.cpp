#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>


//53. 最大子数组和

class Solution
{
public:
    int maxSubArray(vector<int>& nums)
    {
        int n = nums.size();
        //1. 创建dp表
        vector<int> dp(n + 1, -0x3f3f3f3f);
        //2. 初始化
        //3. 填表
        int ans = INT_MIN;
        for (int i = 1; i <= n; ++i)
        {
            dp[i] = max(dp[i - 1] + nums[i - 1], nums[i - 1]);
            ans = ans > dp[i] ? ans : dp[i];
        }
        //4. 返回值
        return ans;
    }
};

//152. 乘积最大子数组


class Solution
{
public:
    int maxProduct(vector<int>& nums)
    {
        int n = nums.size();
        if (n == 1) return nums[0];
        //1. 创建dp表
        vector<int> f(n + 1);
        auto g = f;
        //2. 初始化
        f[0] = g[0] = 0;
        //3. 填表
        int ans = 0;
        for (int i = 1; i <= n; ++i)
        {
            f[i] = max(max(f[i - 1] * nums[i - 1], g[i - 1] * nums[i - 1]), nums[i - 1]);
            g[i] = min(min(f[i - 1] * nums[i - 1], g[i - 1] * nums[i - 1]), nums[i - 1]);
            ans = ans > f[i] ? ans : f[i];
        }
        //4. 返回值
        return ans;
    }
};

//918. 环形子数组的最大和

class Solution
{
public:
    int maxSubarraySumCircular(vector<int>& nums)
    {
        int n = nums.size();
        //1. 创建dp表  f->max; g->min
        vector<int> f(n);
        auto g = f;
        //2. 初始化
        f[0] = g[0] = nums[0];
        //3. 填表
        int ans, ans_min, sum;
        ans = ans_min = sum = f[0] = g[0] = nums[0];
        for (int i = 1; i < n; ++i)
        {
            f[i] = max(f[i - 1] + nums[i], nums[i]);
            g[i] = min(g[i - 1] + nums[i], nums[i]);
            ans = ans > f[i] ? ans : f[i];
            ans_min = ans_min < g[i] ? ans_min : g[i];
            sum += nums[i];
        }
        //4. 返回值
        if (sum - ans_min == 0) return ans;
        else return max(sum - ans_min, ans);
    }
};
