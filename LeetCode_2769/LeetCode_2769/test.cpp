#define  _CRT_SECURE_NO_WARNINGS 1

//2769. 找出最大的可达成数字

class Solution {
public:
    int theMaximumAchievableX(int num, int t) {
        return num + 2 * t;
    }
};