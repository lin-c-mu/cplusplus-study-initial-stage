#define  _CRT_SECURE_NO_WARNINGS 1
#include<iostream>

using namespace std;
#include<algorithm>

//2739. 总行驶距离

//方法一：模拟
class Solution {
public:
    int distanceTraveled(int mainTank, int additionalTank) {
        int ans = 0;
        while (mainTank >= 5) {
            mainTank -= 5;
            ans += 50;
            if (additionalTank > 0) {
                additionalTank--;
                mainTank++;
            }
        }
        return ans + mainTank * 10;
    }
};

//方法二：数学
class Solution {
public:
    int distanceTraveled(int mainTank, int additionalTank) {
        return 10 * (mainTank + min((mainTank - 1) / 4, additionalTank));
    }
};