#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

#include<cstdlib>
#include<cstdio>


// 1. C/C++ 内存发布

//int globalVar = 1;
//static int staticGlobalVar = 1;
//void Test()
//{
//	static int staticVar = 1;
//	int localVar = 1;
//	int num1[10] = { 1, 2, 3, 4 };
//	char char2[] = "abcd";
//	const char* pChar3 = "abcd";
//	int* ptr1 = (int*)malloc(sizeof(int) * 4);
//	int* ptr2 = (int*)calloc(4, sizeof(int));
//	int* ptr3 = (int*)realloc(ptr2, sizeof(int) * 4);
//	free(ptr1);
//	free(ptr3);
//}

//2. new和delete操作自定义类型

//// C中List链表的创建
//typedef struct MyListNode
//{
//	struct MyListNode* next;
//	int val;
//}MyListNode;
//
//MyListNode* CreatListNode(int n)
//{
//	MyListNode* newnode = (MyListNode*)malloc(sizeof(MyListNode));
//	if (newnode == NULL)
//	{
//		perror("malloc fail:");
//		return NULL;
//	}
//	newnode->next = NULL;
//	newnode->val = n;
//
//	return newnode;
//}

//C++中List单链表的创建
struct MyList
{
	MyList(int val = 0)
		:_next(nullptr)
		,_val(val)
	{}

	MyList* _next;
	int _val;
};

MyList* CreatList(int n)
{
	MyList head(-1);//哨兵位  ---  出栈销毁

	MyList* tail = &head;
	int val;
	std::cout << "请以此输入" << n << "个节点的值:> " << std::endl;
	for (size_t i = 0; i < n; i++)
	{
		std::cin >> val;
		tail->_next = new MyList(val);  // 堆上开辟，链表实体; 且自动调用构造函数
		tail = tail->_next;
	}
	//返回哨兵位后面一个节点
	return head._next;
}

void func()
{
	int n = 1;
	while (1)
	{
		int* p = new int[1024 * 1024 * 100];
		//int* p = (int*)malloc(1024 * 1024*4);

		std::cout << n << "->" << p << std::endl;
		++n;
	}
}

int main()
{
	//1.用法上，变简洁了
	int* p0 = (int*)malloc(sizeof(int));
	int* p1 = new int;
	int* p2 = new int[10]; // new 10 个int 对象

	delete p1;
	delete[] p2;

	//2、可以控制初始化
	int* p3 = new int(10); //动态申请一个int类型的空间
	int* p4 = new int[10] {1, 2, 3, 4, 5};  //动态申请十个int类型的空间并初始化为{...}， 其余为0

	delete p3;
	delete[] p4;

	// 3、自定义类型，开空间+构造函数
	// 4、new失败了以后抛异常，不需要手动检查


	MyList* list1 = CreatList(5);

	//MyListNode* head = CreatListNode(1);


	////new的异常捕获
	//try
	//{
	//	func();
	//}
	//catch(const std::exception& e)
	//{
	//	std::cout << e.what() << std::endl;
	//}

	return 0;
}


//class A
//{
//public:
//	A(int a = 0)
//		: _a(a)
//	{
//		std::cout << "A():" << this << std::endl;
//	}
//	~A()
//	{
//		std::cout << "~A():" << this << std::endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	// new/delete 和 malloc/free最大区别是 new/delete对于【自定义类型】除了开空间
//	A* p1 = (A*)malloc(sizeof(A));
//	A* p2 = new A;
//	A* p3 = new A[10];
//	//delete p3;//不匹配， 将报错，可能内存泄漏
//	delete[] p3;
//	free(p1);
//	delete p2;
//	return 0;
//}

////delete = 先析构 + operator delete
//class MyStack
//{
//public:
//	MyStack()
//		: _a((int*)malloc(sizeof(int)* 4))
//		,_capacity(4)
//		, _top(0)
//	{}
//
//	~MyStack()
//	{
//		free(_a);
//		_top = _capacity = 0;
//	}
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//};
//
//int main()
//{
//	MyStack* st = new MyStack;
//	delete st;
//	return 0;
//}
