#define  _CRT_SECURE_NO_WARNINGS 1

//1553. 吃掉 N 个橘子的最少天数

//方法二：最短路
using PII = pair<int, int>;

class Solution {
public:
    int minDays(int n) {
        priority_queue<PII, vector<PII>, greater<PII>> q;
        unordered_set<int> visited;
        q.emplace(0, n);
        int ans = 0;
        while (true) {
            auto [days, rest] = q.top();
            q.pop();
            if (visited.count(rest)) {
                continue;
            }
            visited.insert(rest);
            if (rest == 1) {
                ans = days + 1;
                break;
            }
            q.emplace(days + rest % 2 + 1, rest / 2);
            q.emplace(days + rest % 3 + 1, rest / 3);
        }
        return ans;
    }
};

//方法三：启发式搜索
using TIII = tuple<int, int, int>;

class Solution {
public:
    int minDays(int n) {
        auto getHeuristicValue = [](int rest) -> int {
            return rest == 0 ? 0 : \
                static_cast<int>(log(static_cast<double>(rest)) / log(3.)) + 1;
        };
        auto compareFn = [](const TIII& u, const TIII& v) {
            return get<0>(u) + get<1>(u) > get<0>(v) + get<1>(v);
        };
        priority_queue<TIII, vector<TIII>, decltype(compareFn)> q(compareFn);
        unordered_set<int> visited;
        q.emplace(0, getHeuristicValue(n), n);
        int ans = 0;
        while (true) {
            auto [days, heuristic, rest] = q.top();
            q.pop();
            if (visited.count(rest)) {
                continue;
            }
            visited.insert(rest);
            if (rest == 1) {
                ans = days + 1;
                break;
            }
            q.emplace(days + rest % 2 + 1, getHeuristicValue(rest / 2), rest / 2);
            q.emplace(days + rest % 3 + 1, getHeuristicValue(rest / 3), rest / 3);
        }
        return ans;
    }
};