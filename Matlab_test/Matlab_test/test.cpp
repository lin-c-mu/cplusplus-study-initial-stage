#define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
#include <stdio.h>

int main()
{
	int loop = 9;
	FILE* pfile;
	pfile = fopen("test.txt", "r");
	while (loop--)
	{
		double result = 0.0;
		std::vector<double> x(10);
		std::vector<double> b = { 0.0132, 0.0022, -0.0092, 0.0327, -0.0004, -0.0055, 0.0244, 0.0859, -0.0087, 0.0019 };
		int loop = 10;
		for (int i = 0; i < 10; i++)
		{
			fscanf(pfile, "%lf", &x[i]);
			result += x[i] * b[i];
		}
		for (int i = 0; i < 10; i++)
			std::cout << x[i] << std::endl;
		result += 0.7730;
		std::cout << result << std::endl;
	}
	return 0;
}