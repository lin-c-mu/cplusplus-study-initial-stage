#define  _CRT_SECURE_NO_WARNINGS 1

//43. 字符串相乘

class Solution
{
public:
    string addStrings(string num1, string num2)
    {
        string anr;
        size_t len1 = num1.size();
        size_t len2 = num2.size();
        size_t loop_count = len1 < len2 ? len1 : len2;
        size_t cur = len1 > len2 ? len1 : len2;
        anr.resize(cur + 1);
        anr[cur + 1] = '\0';
        int flag = 0;
        for (int i = loop_count - 1; i >= 0; --i)
        {
            anr[cur] = num1[len1 - 1] + num2[len2 - 1] + flag - '0';
            flag = 0;
            --len1;
            --len2;
            if (anr[cur] > '9')
            {
                anr[cur] -= 10;
                flag = 1;
            }
            --cur;
        }
        if (len1 != 0)
        {
            while (cur)
            {
                anr[cur] = num1[len1 - 1] + flag;
                --len1;
                flag = 0;
                if (anr[cur] > '9')
                {
                    anr[cur] -= 10;
                    flag = 1;
                }
                --cur;
            }
        }
        if (len2 != 0)
        {
            while (cur)
            {
                anr[cur] = num2[len2 - 1] + flag;
                --len2;
                flag = 0;
                if (anr[cur] > '9')
                {
                    anr[cur] -= 10;
                    flag = 1;
                }
                --cur;
            }
        }
        if (flag == 1)
            anr[0] = '1';
        else
            anr.erase(0, 1);

        return anr;
    }

    string multiply(string num1, string num2)
    {
        int len1 = num1.size();
        string sum("0");
        string anr("0");
        int n = 0;
        if (num1 == "0" || num2 == "0")
            return anr;
        while (len1 > 0)
        {
            int count = num1[len1 - 1] - '0';
            while (count--)
            {
                sum = addStrings(sum, num2);
            }
            for (int i = 0; i < n; ++i)
            {
                sum.push_back('0');
            }
            n++;
            anr = addStrings(anr, sum);
            sum = "0";
            len1--;
        }
        return anr;
    }
};

//557. 反转字符串中的单词 III

class Solution
{
public:
    string reverseWords(string s)
    {
        size_t pos = s.find(' ');
        size_t left = 0, right = pos - 1;
        while (pos != string::npos)
        {
            while (left < right)
                swap(s[left++], s[right--]);
            left = pos + 1;
            pos = s.find(' ', pos + 1);
            right = pos - 1;
        }
        right = s.size() - 1;
        while (left < right)
            swap(s[left++], s[right--]);
        return s;
    }
};

//HJ1 字符串最后一个单词的长度

#include <iostream>
using namespace std;

int main()
{
    string s;
    getline(cin, s);
    size_t pos = s.rfind(' ');
    cout << s.size() - pos - 1 << endl;
}

//541. 反转字符串 II

class Solution
{
public:
    string reverseStr(string s, int k)
    {
        int size = s.size(), count = 0;
        while (count <= size)
        {
            int left = count, right = count + k - 1;
            if (right + 1 > s.size())
                right = s.size() - 1;
            while (left < right)
                swap(s[left++], s[right--]);
            count += 2 * k;
        }
        return s;
    }
};