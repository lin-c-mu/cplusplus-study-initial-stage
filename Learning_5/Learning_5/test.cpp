#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

void test_string1()
{
	string  s0;
	string s1("hello world");
	string s2(s1);
	string s3(s1, 5, 3);
	string s4(s1, 5, 10);
	string s5(s1, 5);
	string s6(10, '#');

	cout << s0 << endl;
	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;
	cout << s5 << endl;
	cout << s6 << endl;

	s0 = s6;
}

//[] 的重载大致逻辑

//class string
//{
//public:
//	char& operator[](size_t pos)
//	{
//		return _str[pos];
//	}
//private:
//	char* _str;
//	int _size;
//	int _capacity;
//};

void test_string2()
{
	string s1("hello world");

	//遍历字符串string

	//下标+方括号
	//
	//for (size_t i = 0; i < s1.size(); i++)
	//{
	//	s1[i]++;
	//}
	//cout << endl;
	//
	//for (size_t i = 0; i < s1.size(); i++)
	//{
	//	cout << s1[i] << " ";
	//	//cout << s1.operator[](i) << " ";
	//}
	//cout << endl;
	//
	//const string s2("hello world");
	//
	//for (size_t i = 0; i < s2.size(); i++)
	//{
	//	s2[i]++;
	//}
	//cout << endl;
	//
	//for (size_t i = 0; i < s2.size(); i++)
	//{
	//	cout << s2[i] << " ";
	//	//cout << s1.operator[](i) << " ";
	//}
	//cout << endl;

	//迭代器 ，行为像指针一样的类型对象
	//string::iterator it1 = s1.begin();
	//while (it1 != s1.end())
	//{
	//	cout << *it1 << " ";
	//	++it1;
	//}
	//cout << endl;
	//
	////cout << typeid(it1).name() << endl;

	//范围for
	//但底层就是迭代器
	//for (auto e : s1)
	//{
	//	cout << e << " ";
	//}
	//cout << endl;
}

//Iterator
void test_string3()
{
	

	string s1("hello cplusplus");
	const string s2("hello world");

	//反向迭代器

	//string::reverse_iterator rit = s1.rbegin();
	//
	//while (rit != s1.rend())
	//{
	//	cout << *rit << ' ';
	//	++rit;
	//}
	//cout << endl;

	//可读可写
	string::iterator it1 = s1.begin();
	while (it1 != s1.end())
	{
		cout << *it1 << " ";
		++it1;
	}
	cout << endl;

	//只读
	string::const_iterator it = s2.begin();
	while (it != s2.end())
	{
		cout << *it << ' ';
		++it;
	}
	cout << endl;
}

//Capacity
void test_string4()
{
	cout << "test_string4():" << endl << endl;

	string s1("hello world");
	//推荐使用size
	cout << s1.size() << endl;
	cout << s1.length() << endl;

	//cout << s1.max_size() << endl;
	cout << s1.capacity() << endl;
	cout << "--------------------------" << endl;


	//检验capacity增长变化（扩容机制）--- 注意比较Linux下，g++编译器扩容机制
	string s;
	size_t sz = s.capacity();
	cout << "capacity changed: " << sz << '\n';
	cout << "making s grow:\n";
	for (size_t i = 0; i < 100; ++i)
	{
		s.push_back('c');
		if (sz != s.capacity())
		{

			sz = s.capacity();
			cout << "capacity changed: " << sz << '\n';
		}
	}
	cout << "--------------------------" << endl;


	//s1.clear() -- 只会清除数据，capacity不变
	//s1.shrink_to_fit() -- 缩容到size，最小为15（具体看编译器）
	string s2("hello xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
	cout << "size:" << s2.size() << endl;
	cout << "capacity:" << s2.capacity() << endl << endl;
	cout << "after use s.clear() and s.shrink_to_fit()" << endl;

	s2.clear();
	s2.shrink_to_fit();
	cout << "size:" << s2.size() << endl;
	cout << "capacity:" << s2.capacity() << endl;
	cout << "--------------------------" << endl;


	//s1.reserve(size_t) -- 预留空间，且做不到缩容，即size_t > s.capapcity
	//一般用作：知道要插入多少数据，提前开好空间。避免了扩容，提高了效率
	string s3;
	s3.reserve(100);
	cout << "after used s.reserver(100), capacity: " << s3.capacity() << endl;
	cout << "--------------------------" << endl;


	//s1.resize(size_t, char) -- if(size_t < s1.size()) -> 删除; elseif(size_t > s1.size() && size_t < capacity) -> 插入
	//elseif(size_t > capacity) -> 插入+扩容
	string s5("hello worldxxxxxxx");
	string s4(s5);
	cout << s4 << endl;
	cout << "size:" << s4.size() << endl;
	cout << "capacity:" << s4.capacity() << endl << endl;

	s4.resize(10);
	cout << "after s.resize(10) && size_t < s.size():" << endl;
	cout << s4 << endl;
	cout << "size:" << s4.size() << endl;
	cout << "capacity:" << s4.capacity() << endl << endl;

	s4 = s5;
	s4.resize(20,'c');
	cout << "after s.resize(20) && size_t < s.size():" << endl;
	cout << s4 << endl;
	cout << "size:" << s4.size() << endl;
	cout << "capacity:" << s4.capacity() << endl << endl;

	s4 = s5;
	s4.resize(35, 'l');
	cout << "after s.resize(35) && size_t < s.size():" << endl;
	cout << s4 << endl;
	cout << "size:" << s4.size() << endl;
	cout << "capacity:" << s4.capacity() << endl;

}

//Element access
void test_string5()
{
	cout << "test_string5():" << endl << endl;

	//[] && s.at() -- 两者主要区别在于，访问非法位置报错不同。s.at(): 异常信息可被捕获
	string s1("hello world");
	cout << s1[2] << endl;
	cout << s1.at(2) << endl;
	try
	{
		//s1[20];
		s1.at(20);
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}

	//s.front() && s.back()
}

//Modifiers
void test_string6()
{
	cout << "test_string6():" << endl << endl;

	string s1("hello world");

	//s.push_back() -- 尾插
	s1.push_back('!');
	cout << s1 << endl;
	cout << "--------------------------" << endl;


	//s.append() 
	s1.append(" hello bit");
	cout << s1 << endl;

	s1.append(10, 'x');
	cout << s1 << endl;

	string s2("  orange  ");
	//s1.append(s2);
	//cout << s1 << endl;
	//去首尾两空格
	s1.append(++s2.begin(), --s2.end());
	cout << s1 << endl;
	cout << "--------------------------" << endl;


	//operator+=
	string s3("hello world");
	s3 += ' ';
	s3 += "orange";
	cout << s3 << endl;
	cout << "--------------------------" << endl;


	//s.assign() -- 覆盖
	string s4("hello world");
	cout << "size:" << s4.size() << endl;
	s4.assign("xxxx");
	cout << s4 << endl;
	cout << "after assigned, size:" << s4.size() << endl;
	cout << "--------------------------" << endl;


	//s.insert()
	string s5("hello world");
	s5.insert(2, "xxx");
	cout << s5 << endl;

	s5.insert(1, s4);
	cout << s5 << endl;
	cout << "--------------------------" << endl;


	//s.erase -- s.erase(size_t pos = 0, size_t len = npos);注意缺省值



	//s.replace() -- 替换
	string s7("hello world! hello Linux!");

	//替换空格
	//size_t pos = s7.find(' ');
	//while (pos != string::npos)
	//{
	//	s7.replace(pos, 1, "^.^");
	//	pos = s7.find(' ');
	//	//pos = s7.find(pos + 5, ' ');
	//}
	//cout << s7 << endl;

	//insert / erase / replace  --  尽量少用
	//因为基本都要挪动数据，效率不是很高
	string s8;
	s8.reserve(s7.size());
	for (auto ch : s7)
	{
		if (ch != ' ')
			s8 += ch;
		else
			s8 += " ^.^ ";
	}
	cout << s8 << endl;
	s7.swap(s8);
	cout << s7 << endl;
}

int main()
{
	//test_string1();
	//test_string2();
	//test_string3();
	//test_string4();
	//test_string5();
	test_string6();

	return 0;
}