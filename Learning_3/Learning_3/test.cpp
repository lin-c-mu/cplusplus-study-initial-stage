#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

//模板

//函数模板
template<typename T>
//多个 template<typename T1, typename T2>
// 编译器通过推出类型，用函数模板，生成对应的函数，这个过程叫做模板实例化
void Swap(const T& x, const T& y)
{
	T tmp = x;
	x = y;
	y = tmp;
}

template<typename T2>

T2 Add(const T2 x, const T2 y)
{
	return x + y;
}

int main()
{
	//int a = 1, b = 2;
	//Swap(a, b);
	//cout << a << "  " << b << endl;

	//double d1 = 1.1, d2 = 2.2;
	//Swap(d1, d2);
	//cout << d1 << "  " << d2 << endl;

	int a = 1, b = 2;
	double d1 = 1.1, d2 = 2.2;
	//显示转换
	cout << Add<int>(a, d1) << endl;
	cout << Add<double>(a, d1) << endl;
	
	
	return 0;
}