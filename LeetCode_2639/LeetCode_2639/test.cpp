#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<algorithm>
#include<vector>

//2639. 查询网格图中每一列的宽度

//手动计算长度的代码
class Solution {
public:
    vector<int> findColumnWidth(vector<vector<int>>& grid) {
        int n = grid.size(), m = grid[0].size();
        vector<int> res(m);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int x = grid[i][j];
                int length = 0;
                if (x <= 0) {
                    length = 1;
                }
                while (x != 0) {
                    length += 1;
                    x /= 10;
                }
                res[j] = max(res[j], length);
            }
        }
        return res;
    }
};

//转为字符串后直接获取长度的代码
class Solution {
public:
    vector<int> findColumnWidth(vector<vector<int>>& grid) {
        int n = grid.size(), m = grid[0].size();
        vector<int> res(m);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                res[j] = max(res[j], (int)to_string(grid[i][j]).size());
            }
        }
        return res;
    }
};