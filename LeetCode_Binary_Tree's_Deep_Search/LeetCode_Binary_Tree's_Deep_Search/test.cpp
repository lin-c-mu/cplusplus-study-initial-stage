#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//2331. 计算布尔二叉树的值

class Solution
{
public:
    bool evaluateTree(TreeNode* root)
    {
        if (root->left == nullptr && root->right == nullptr)
            return root->val == 0 ? false : true;

        bool l = evaluateTree(root->left);
        bool r = evaluateTree(root->right);

        return root->val == 2 ? l | r : l & r;
    }
};

//129. 求根节点到叶节点数字之和

class Solution
{
public:
    int sumNumbers(TreeNode* root)
    {
        return dfs(root, 0);
    }
    int dfs(TreeNode* root, int n)
    {
        if (root == nullptr) return 0;

        int prevSum = n * 10 + root->val;
        if (root->left == nullptr && root->right == nullptr) return prevSum;
        return dfs(root->left, prevSum) + dfs(root->right, prevSum);
    }
};

//814. 二叉树剪枝

class Solution
{
public:
    TreeNode* pruneTree(TreeNode* root)
    {
        if (root == nullptr) return root;
        root->left = pruneTree(root->left);
        root->right = pruneTree(root->right);
        if (root->left == nullptr && root->right == nullptr && root->val == 0)
            root = nullptr;
        return root;
    }
};

//98. 验证二叉搜索树

class Solution
{
    long judge = LONG_MIN;
public:
    bool isValidBST(TreeNode* root)
    {
        if (root == nullptr) return true;

        //剪枝
        bool l = isValidBST(root->left);
        if (l == false) return false;

        bool cur = false;
        if (judge < root->val)
            cur = true;
        judge = root->val;
        bool r = isValidBST(root->right);
        //剪枝
        if (r == false) return false;
        return l && r && cur;
    }
};