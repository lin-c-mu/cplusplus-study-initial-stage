#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

#include<vector>
#include<queue>

//2462. 雇佣 K 位工人的总代价


class Solution {
public:
    long long totalCost(vector<int>& costs, int k, int candidates) {
        int n = costs.size();
        priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;
        int left = candidates - 1, right = n - candidates;
        if (left + 1 < right) {
            for (int i = 0; i <= left; ++i) {
                q.emplace(costs[i], i);
            }
            for (int i = right; i < n; ++i) {
                q.emplace(costs[i], i);
            }
        }
        else {
            for (int i = 0; i < n; ++i) {
                q.emplace(costs[i], i);
            }
        }
        long long ans = 0;
        for (int _ = 0; _ < k; ++_) {
            auto [cost, id] = q.top();
            q.pop();
            ans += cost;
            if (left + 1 < right) {
                if (id <= left) {
                    ++left;
                    q.emplace(costs[left], left);
                }
                else {
                    --right;
                    q.emplace(costs[right], right);
                }
            }
        }
        return ans;
    }
};