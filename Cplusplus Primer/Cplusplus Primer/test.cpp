#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<map>

//1.命名空间

//namespace lzw
//{
//	int rand = 10;
//}
//
//namespace myz
//{
//	//变量
//	int rand = 20;
//
//	//函数
//	int Add(int a, int b)
//	{
//		return a + b;
//	}
//
//	//结构体
//	struct Student
//	{
//		int age;
//		char name[128];
//	};
//}
//
//int main()
//{
//	printf("%d\n", lzw::rand);
//	printf("%d\n", myz::rand);
//
//	int ret = myz::Add(2, 3);
//	printf("%d\n", ret);
//	return 0;
//}

//------------------------------------------------------------------------------

//2. 输入 & 输出

//int main()
//{
//
//	//1. 左移
//	int i = 1;
//	i = i << 2;
//
//	//2. 输出
//	cout << i << endl;
//
//	int n;
//	cin >> n; // 输入
//	return 0;
//}

//------------------------------------------------------------------------------

//3. 缺省参数

////全缺省
////void Func(int a = 1, int b = 2, int c = 3)
////{
////	cout << "a= " << a << endl;
////	cout << "b= " << b << endl;
////	cout << "c= " << c << endl;
////}
//
////半缺省 --- 从右往左连续给
//void Func(int a, int b = 2, int c = 3)
//{
//	cout << "a= " << a << endl;
//	cout << "b= " << b << endl;
//	cout << "c= " << c << endl;
//}
//
//struct Stack
//{
//	int* a;
//	int size;
//	int capacity;
//};
//
//void StackInit(struct Stack* st, int n = 4)
//{
//	st->a = (int*)malloc(sizeof(int) * n);
//}
//
//int main()
//{
//	Func(10);
//	Func(10, 20, 30);
//
//	//1. 确定要插入100个数据
//	struct Stack st1;
//	StackInit(&st1, 100);
//
//	//2. 确定要插入10个数据
//	struct Stack st2;
//	StackInit(&st2, 10);
//
//	//3. 不确定插入数据个数  -- 使用缺省值
//	struct Stack st3;
//	StackInit(&st3);
//
//	return 0;
//}

//------------------------------------------------------------------------------

//4.函数重载

//int Add(int a, int b)
//{
//	return a + b;
//}
//
//double Add(double a, double b)
//{
//	return a + b;
//}
//
//int main()
//{
//	int retI = Add(11, 22);
//	double retD = Add(1.1, 2.2);
//	cout << "retI: " << retI << "; retD: " << retD << endl;
//	return 0;
//}

//------------------------------------------------------------------------------

//5. 引用

//void TestRef()
//{
//	//同一段空间
//	int a = 10;
//	int& ra = a;
//	cout << "address a  : " << &a << endl;
//	cout << "address ra : " << &ra << endl;
//}
//
//void TestRef2()
//{
//	int a = 10;
//	// int& ra;   // 该条语句编译时会出错
//	int& ra = a;
//	int& rra = ra;
//	ra++;
//	cout << "a: " << a << "; ra: " << ra << "; rra: " << rra << endl; //同样指向同一块地址空间
//}
//
//void TestRef3()
//{
//
//	int a = 10;
//	//1. 引用必须初始化
//	int& ra = a;
//	
//	//2. 不能改变指向，
//	int c = 20;
//	ra = c; // => a = ra = 20; 变为赋值操作
//
//	//3. 一个变量可以有多个引用
//}
//
////a、引用做参数
//typedef struct ListNode
//{
//	struct ListNode* prev;
//	struct ListNode* next;
//	int val;
//}LNode, *PNode;
//
////void PushBack(struct ListNode** pphead, int val);
//void PushBack(struct ListNode*& phead, int val)
//{
//	struct ListNode* newnode = new struct ListNode;
//	phead = newnode;
//}
//
//void Insert(PNode& phead, int val)
//{
//
//}
//
////b、做输出值
//
//int main()
//{
//	TestRef2();
//
//	struct ListNode* pHead = nullptr;
//	PNode plist = nullptr;
//
//
//	return 0;
//}

//------------------------------------------------------------------------------

//6.内联函数inline

//inline int Add(int x, int y)
//{
//
//	return x + y;
//}
//
//int main()
//{
//	//调用的地方展开 -- 逻辑实现不变
//	int ret = Add(10, 20);
//	return 0;
//}

//------------------------------------------------------------------------------

//7. auto 关键字 (C++11)

//void Func(int x, int y)
//{
//	cout << x + y << endl;
//}
//
//int main()
//{
//	int i = 0;
//	auto j = 2;
//
//	auto p = i;
//	auto ptr = &i;
//	auto* ptr1 = &i;
//
//	void(*pf)(int, int) = Func;
//	auto pf1 = Func;
//	cout << typeid(pf).name() << endl;
//	cout << typeid(pf1).name() << endl;
//
//	std::map<std::string, std::string> dict;
//	//std::map<std::string, std::string>::iterator it = dict.begin();
//	auto it1 = dict.end();
//
//	return 0;
//}

//------------------------------------------------------------------------------

//8. 基于范围的for循环 (C++11)

//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5 };
//	//不可变  -> 1 2 3 4 5
//	for (auto n : arr)
//		cout << n << " ";
//	//引用，可变 -> 2 4 6 8 10
//	for (auto& n : arr)
//	{
//		n *= 2;
//		cout << n << " ";
//	}
//	return 0;
//}

//------------------------------------------------------------------------------

//9. nullptr (C++11)

void f(int i)
{
	cout << "f(int)" << endl;
}

void f(int* p)
{
	cout << "f(int*)" << endl;
}

int main()
{
	f(0);
	f(NULL);
	f((int*)NULL);
	f(nullptr);

	return 0;
}