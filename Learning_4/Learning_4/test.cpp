#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<string>

using namespace std;

////函数模板
//template<typename T>
//void Swap(T& a, T& b)
//{
//	T tmp = a;
//	a = b;
//	b = tmp;
//
//}
//
////类模板
//template<class T>
//class Stack
//{
//public:
//	Stack(T* a = NULL, int size = 0, int capacity = 4)
//		:_a = a,
//		_size = size,
//		_capacity = capacity
//	{
//
//	}
//	void Push(const T& a)
//	{
//		//...
//	}
//
//private:
//	T* _a;
//	int _size;
//	int _capacity;
//};
//
//int main()
//{
//	int a = 1, b = 4;
//	Swap(a, b);
//	cout << a << "  " << b << endl;
//	swap(a, b);//c++ 提供交换函数
//	cout << a << "  " << b << endl;
//
//	//同一个类模板实例化出的两个类型
//	Stack<int> s1;
//	Stack<double> s2;
//	return 0;
//}

void test_string1()
{
	string  s0;
	string s1("hello world");
	string s2(s1);
	string s3(s1, 5, 3);
	string s4(s1, 5, 10);
	string s5(s1, 5);
	string s6(10, '#');

	cout << s0 << endl;
	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;
	cout << s5 << endl;
	cout << s6 << endl;

	s0 = s6;
}

void test_string2()
{
	//遍历字符串string

	//下标+方括号
	//string s1("hello world");
	//
	//for (size_t i = 0; i < s1.size(); i++)
	//{
	//	s1[i]++;
	//}
	//cout << endl;
	//
	//for (size_t i = 0; i < s1.size(); i++)
	//{
	//	cout << s1[i] << " ";
	//	//cout << s1.operator[](i) << " ";
	//}
	//cout << endl;
	//
	//const string s2("hello world");
	//
	//for (size_t i = 0; i < s2.size(); i++)
	//{
	//	s2[i]++;
	//}
	//cout << endl;
	//
	//for (size_t i = 0; i < s2.size(); i++)
	//{
	//	cout << s2[i] << " ";
	//	//cout << s1.operator[](i) << " ";
	//}
	//cout << endl;

	string s1("hello world");

	//迭代器 ，行为像指针一样的类型对象
	//string::iterator it1 = s1.begin();
	//while (it1 != s1.end())
	//{
	//	cout << *it1 << " ";
	//	++it1;
	//}
	//cout << endl;
	//
	////cout << typeid(it1).name() << endl;

	//范围for
	//但底层就是迭代器
	for (auto e : s1)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	test_string2();
	return 0;
}