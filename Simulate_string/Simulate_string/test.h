#define  _CRT_SECURE_NO_WARNINGS 1

#include<string.h>
#include<assert.h>
#include<iostream>

using namespace std;


#pragma once



namespace lzw_string
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;

		static const int npos;


		string(const char* str = "")
			:_size(strlen(str))
		{
			_capacity = _size;
			_str = new char[_size + 1];
			strcpy(_str, str);
		}


		//  s2(s1)  -- 避免浅拷贝（造成两个string类指向同一块空间）， 重复释放
		string(const string& s)
		{
			_str = new char[s._capacity + 1];
			strcpy(_str, s._str);

			_size = s._size; 
			_capacity = s._capacity;
		}


		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}


		const char* c_str() const
		{
			return _str;
		}


		size_t size() const
		{
			return _size;
		}


		size_t capacity() const
		{
			return _capacity;
		}


		string& operator= (const char* str)
		{
			size_t len = strlen(str);
			_str = new char[len+1];

			strcpy(_str, str);
			_size = len;
			_capacity = len;
			return *this;
		}


		string& operator= (const string& s)
		{
			char* tmp = new char[s.capacity() + 1];
			strcpy(tmp, s._str);

			delete[] _str;
			_str = tmp;
			_size = s._size;
			_capacity = s._capacity;

			return *this;
		}


		char& operator[] (size_t pos)
		{
			assert(pos < _size);

			return _str[pos];
		}


		const char& operator[] (size_t pos) const
		{
			assert(pos < _size);

			return _str[pos];
		}


		const_iterator begin() const
		{
			return _str;
		}


		const_iterator end() const
		{
			return _str + _size;
		}


		iterator begin()
		{
			return _str;
		}


		iterator end()
		{
			return _str + _size;
		}


		void resize(size_t n, char ch = '\0')
		{
			// < _size  删除
			if (n <= _size)
			{
				_str[n] = '\0';
				_size = n;
			}
			// > _size && < _capacity 插入； >_capacity 插入+扩容   -- 合二为一
			else
			{
				reserve(n);
				while (_size < n)
				{
					_str[_size++] = ch;
				}
				_str[_size] = '\0';
			}
		}

		//开辟更大空间
		void reserve(size_t n = 0)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				tmp[n] = '\0';
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}


		void push_back(char ch)
		{
			if (_size == _capacity)
				reserve(_capacity == 0 ? 4 : 2 * _capacity);

			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
			//insert(_size, ch)

		}


		string& append(const char* s)
		{
			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}

			strcpy(_str + _size, s);
			_size += len;
			return *this;
			//insert(_size, s);
		}


		string& operator+= (char ch)
		{
			push_back(ch);
			return *this;
		}


		string& operator+= (const char* str)
		{
			append(str);
			return *this;
		}

		//pos位置 插入字符
		void insert(size_t pos, char ch)
		{
			assert(pos <= _size);

			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : 2 * _capacity);
			}

			size_t n = _size + 1;
			while (n > pos)
			{
				_str[n] = _str[n - 1];
				--n;
			}

			_str[pos] = ch;
			++_size;
		}

		//pos位置 插入字符串
		void insert(size_t pos, const char* s)
		{
			assert(pos <= _size);

			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			size_t n = _size + len;
			size_t prev = _size + 1;
			while (prev > pos)
			{
				_str[n] = _str[prev - 1];
				--prev;
				--n;
			}
			/*for (size_t i = pos; i < pos + len; ++i)
			{
				_str[i] = s[i - pos];
			}*/
			strncpy(_str + pos, s, len);

		}


		void erase(size_t pos = 0, size_t len = npos)
		{
			assert(pos < _size);

			if (len == npos || len > _size - pos)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
			
		}

		//交换两个string对象 的成员
		// 减少了 三次拷贝 + 一次析构
		// 因为std库中 swap() 函数 需要额外构造一个string
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		//找字符 -- 遍历[pos, _size)
		size_t find(char ch, size_t pos = 0)
		{
			assert(pos < _size);

			for (size_t i = pos; i < _size; ++i)
			{
				if (_str[i] == ch)
					return i;
			}
			return npos;
		}

		//找字符串 -- strstr
		size_t find(const char* sub, size_t pos = 0)
		{
			assert(pos < _size);

			char* p = strstr(_str + pos, sub);
			if (p)
			{
				return p - _str;
			}
			else
			{
				return npos;
			}
		}

		string substr(size_t pos = 0, size_t len = npos)
		{
			string anr;
			if (len >= _size - pos)
			{
				anr.reserve(_size - pos);
				strcpy(anr._str, _str + pos);
				anr._size = _size - pos;
			}
			else
			{
				anr.reserve(len);
				strncpy(anr._str, _str + pos, len);
				anr._size = len;
			}
			return anr;
		}

		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};

	const int string::npos = -1;

	//全局swap 调用string中swap 为了避免使用到std中的swap
	//因为std中swap()函数为 模板 。 所以会优先调用此处的swap();
	void swap(string& s1, string& s2)
	{
		s1.swap(s2);
	}

	ostream& operator<< (ostream& out, const string& s)
	{
		//out << s.c_str();
		for (auto ch : s)
		{
			out << ch;
		}
		return out;
	}

	istream& operator>> (istream& in, string& s)
	{
		s.clear();
		char ch = in.get();
		size_t i = 0;
		char buf[128];
		while (ch != ' ' && ch != '\n')
		{
			//浪费空间，效率低，因为需要不断扩容
			/*s += ch;
			ch = in.get();*/

			
			buf[i++] = ch;
			ch = in.get();
			if (i == 127)
			{
				buf[i] = '\0';
				i = 0;
				s += buf;
			}
		}
		if (i > 0)
		{
			buf[i] = '\0';
			s += buf;
		}
		return in;
	}

	bool operator== (const string& s1, const string& s2)
	{
		int cmp = strcmp(s1.c_str(), s1.c_str());
		return cmp == 0;
	}

	bool operator< (const string& s1, const string& s2)
	{
		int cmp = strcmp(s1.c_str(), s1.c_str());
		return cmp < 0;
	}

	bool operator> (const string& s1, const string& s2)
	{
		int cmp = strcmp(s1.c_str(), s1.c_str());
		return cmp > 0;
	}

	bool operator>= (const string& s1, const string& s2)
	{
		return (s1 == s1) || (s1 > s2);
	}

	bool operator<= (const string& s1, const string& s2)
	{
		return (s1 == s1) || (s1 < s2);
	}

	bool operator!= (const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}

	istream& getline(istream& in, string& s)
	{
		s.clear();

		char ch;
		//in >> ch;
		ch = in.get();
		char buff[128];
		size_t i = 0;
		while (ch != '\n')
		{
			buff[i++] = ch;
			// [0,126]
			if (i == 127)
			{
				buff[127] = '\0';
				s += buff;
				i = 0;
			}

			ch = in.get();
		}

		if (i > 0)
		{
			buff[i] = '\0';
			s += buff;
		}

		return in;
	}

	void test_string1()
	{
		string s1("hello world");
		string s2;
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << ++s1[i] << " ";
		}
		cout << endl;

		const string s3("nice to meet you!");
		for (size_t i = 0; i < s3.size(); i++)
		{
			cout << s3[i] << " ";
		}
		cout << endl;

		string s4 = "lzw love xxx";
		for (size_t i = 0; i < s4.size(); i++)
		{
			cout << s4[i] << " ";
		}
		cout << endl;
	}

	void test_string2()
	{
		string s1("hello Linux");

		string::iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
	}

	void test_string3()
	{
		string s1 = "hello world!";
		string s2("lzw");
		cout << "s2: " << s2.c_str() << endl;
		s2 = s1;
		cout << "s2: " << s2.c_str() << endl;

		s1.push_back('x');
		s1.append("yyyyyy");
		cout << "s1: " << s1.c_str() << endl;

		s2 += 'q';
		s2 += 'q';
		s2 += 'q';
		s2 += 'q';
		s2 += "wwwwwwwww";
		cout << s2.c_str() << endl;
	}

	void test_string4()
	{
		string s1("lzw love");
		s1.insert(0, 'x');
		cout << s1.c_str() << endl;
		s1.insert(0, "mu ");
		cout << s1.c_str() << endl;
		cout << "-----------------" << endl;


		string s2 = "hello world!";
		s2.erase(4);
		cout << s2.c_str() << endl;
		s2 = "qwwwwwwwasghdfgh";
		s2.erase(3, 5);
		cout << s2.c_str() << endl;
		cout << "-----------------" << endl;
	}

	void test_string5()
	{
		string s1("hello world!");
		string s2(s1);
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

		//s1[0] = 'x';
		//cout << s1.c_str() << endl;
		//cout << s2.c_str() << endl;
	}

	void test_string6()
	{
		string s1("hello world!");
		cout << s1.c_str() << endl;
		cout << "_size = " << s1.size() << endl;
		cout << "_capacity = " << s1.capacity() << endl;
		cout << "-----------------" << endl;

		s1.resize(100);
		cout << s1.c_str() << endl;
		cout << "After s1.resize(100);" << endl;
		cout << "_size = " << s1.size() << endl;
		cout << "_capacity = " << s1.capacity() << endl;
		cout << "-----------------" << endl;


		s1.reserve(200);
		cout << s1.c_str() << endl;
		cout << "After s1.reserve(200);" << endl;
		cout << "_size = " << s1.size() << endl;
		cout << "_capacity = " << s1.capacity() << endl;
	}

	void test_string7()
	{
		string s1("hello");
		string s2("nice to meet you!");

		cout << "s1: " << s1.c_str() << endl;
		cout << "s2: " << s2.c_str() << endl;
		//swap(s1, s2);
		s1.swap(s2);
		cout << "After swap(string&, string&): " << endl;
		cout << "s1: " << s1.c_str() << endl;
		cout << "s2: " << s2.c_str() << endl;
		cout << "-----------------" << endl;


	}

	void test_string8()
	{
		string s1("hello world!");
		cout << s1.c_str() << endl;
		size_t pos = s1.find('l');
		cout << "the pos of char 'l': " << pos << endl;
		cout << "-----------------" << endl;

		size_t pos1 = s1.find("or");
		cout << "the pos of char*  'or': " << pos1 << endl;
		cout << "-----------------" << endl;



		string url("https://legacy.cplusplus.com/reference/string/string/find/");
		
		string protocol, domain, uri;
		size_t pos3 = url.find(':');
		if (pos3 != string::npos)
		{
			protocol = url.substr(0, pos3);
			cout << "protocol: " << protocol << endl;
		}
		size_t pos4 = url.find('/', pos3 + 3);
		if (pos4 != string::npos)
		{
			domain = url.substr(pos3 + 3, pos4 - pos3 - 3);
			cout << "domain: " << domain << endl;
		}
		uri = url.substr(pos4 + 1, url.size() - pos4 - 1);
		//uri = url.substr(pos2 + 1);
		cout << "uri: " << uri << endl;


	}

	void test_string9()
	{
		string s1("helo Linux!");
		string s2(s1);

		cout << "s1 == s2 : " << (s1 == s2) << endl;

		s1 += 'h';
		cout << "After s1 += 'h' s1 != s2 : " << (s1 != s2) << endl;

		cout << "s1 >= s2 : " << (s1 >= s2) << endl;
		cout << "s1 > s2 : " << (s1 > s2) << endl;
		cout << "s1 < s2 : " << (s1 < s2) << endl;
		cout << "s1 <= s2 : " << (s1 <= s2) << endl;
	}

}

