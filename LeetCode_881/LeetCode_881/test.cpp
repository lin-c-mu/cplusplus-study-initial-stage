#define  _CRT_SECURE_NO_WARNINGS 1

//881. ����ͧ

class Solution {
public:
    int numRescueBoats(vector<int>& people, int limit) {
        int ans = 0;
        sort(people.begin(), people.end());
        int light = 0, heavy = people.size() - 1;
        while (light <= heavy) {
            if (people[light] + people[heavy] > limit) {
                --heavy;
            }
            else {
                ++light;
                --heavy;
            }
            ++ans;
        }
        return ans;
    }
};