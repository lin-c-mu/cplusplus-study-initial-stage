#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

//JZ64 求1+2+3+...+n

class Sum {
public:
    Sum()
    {
        _ret += _i;
        _i++;
    }
    static int GetRet()
    {
        return _ret;
    }
private:
    static int _i;
    static int _ret;
};

int Sum::_i = 1;
int Sum::_ret = 0;

class Solution {
public:
    int Sum_Solution(int n) {
        Sum a[n];
        return Sum::GetRet();
    }
};

//HJ73 计算日期到天数转换

using namespace std;

class Date {
public:
    Date(int year, int month, int day)
        : _year(year)
        , _month(month)
        , _day(day)
    {

    }
    int GetMonthDay(int month)
    {
        int month_day[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        if (month == 2 && ((_year % 4 == 0 && _year % 100 != 0) || _year % 400 == 0))
            return 29;
        else
            return month_day[month];
    }
private:
    int _year;
    int _month;
    int _day;
};

int main() {
    int year, month, day;
    while (cin >> year >> month >> day)
    {
        Date d1(year, month, day);
        int sum = 0;
        for (int i = 1; i < month; i++)
        {
            sum += d1.GetMonthDay(i);
        }
        sum += day;
        cout << sum << endl;
    }

}

//KY222 打印日期

class Date
{
public:
    Date(int year, int month, int day)
        : _year(year)
        , _month(month)
        , _day(day) {
    }

    int GetMonthDay()
    {
        int month_day[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        if (_month == 2 && ((_year % 4 == 0 && _year % 100 != 0) || _year % 400 == 0))
            return 29;
        else
            return month_day[_month];
    }

    void Print()
    {
        if (_month < 10)
        {
            if (_day < 10)
                cout << _year << "-0" << _month << "-0" << _day << endl;
            else
                cout << _year << "-0" << _month << "-" << _day << endl;
        }
        else
        {
            if (_day < 10)
                cout << _year << "-" << _month << "-0" << _day << endl;
            else
                cout << _year << "-" << _month << "-" << _day << endl;
        }
    }

    void Trans()
    {
        while (_day > GetMonthDay())
        {
            _day -= GetMonthDay();
            _month++;
            if (_month == 13)
            {
                _year += 1;
                _month = 1;
            }
        }
    }

private:
    int _year;
    int _month;
    int _day;
};

int main()
{
    int year, day;
    while (cin >> year >> day)
    {
        Date d1(year, 1, day);
        d1.Trans();
        d1.Print();
    }
}