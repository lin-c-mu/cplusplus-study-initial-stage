#define  _CRT_SECURE_NO_WARNINGS 1

//2831. 找出最长等值子数组

//方法一：滑动窗口
class Solution {
public:
    int longestEqualSubarray(vector<int>& nums, int k) {
        int n = nums.size();
        unordered_map<int, vector<int>> pos;
        for (int i = 0; i < n; i++) {
            pos[nums[i]].emplace_back(i);
        }
        int ans = 0;
        for (auto& [_, vec] : pos) {
            /* 缩小窗口，直到不同元素数量小于等于 k */
            for (int i = 0, j = 0; i < vec.size(); i++) {
                while (vec[i] - vec[j] - (i - j) > k) {
                    j++;
                }
                ans = max(ans, i - j + 1);
            }
        }
        return ans;
    }
};

//方法二：一次遍历优化
class Solution {
public:
    int longestEqualSubarray(vector<int>& nums, int k) {
        int n = nums.size();
        int ans = 0;
        unordered_map<int, int> cnt;
        for (int i = 0, j = 0; j < n; j++) {
            cnt[nums[j]]++;
            /*当前区间中，无法以 nums[i] 为等值元素构成合法等值数组*/
            while (j - i + 1 - cnt[nums[i]] > k) {
                cnt[nums[i]]--;
                i++;
            }
            ans = max(ans, cnt[nums[j]]);
        }
        return ans;
    }
};
