#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//69. x 的平方根 

class Solution
{
public:
    int mySqrt(int x)
    {
        int left = 0, right = x;
        while (left <= right)
        {
            int mid = left + (right - left) / 2;
            if ((long long)mid * mid <= x) left = mid + 1;
            else right = mid - 1;
        }
        return left - 1;
    }
};

class Solution
{
public:
    int mySqrt(int x)
    {
        if (x == 0 || x == 1) return x;
        int left = 0, right = x - 1;
        while (left < right)
        {
            int mid = left + (right - left + 1) / 2;
            if ((long long)mid * mid <= x) left = mid;
            else right = mid - 1;
        }
        return left;
    }
};

//35. 搜索插入位置

class Solution
{
public:
    int searchInsert(vector<int>& nums, int target)
    {
        int left = 0, right = nums.size() - 1;
        if (target > nums[right]) return nums.size();
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] >= target) right = mid;
            else left = mid + 1;
        }
        return left;
    }
};

//852. 山脉数组的峰顶索引

class Solution
{
public:
    int peakIndexInMountainArray(vector<int>& arr)
    {
        int left = 0, right = arr.size() - 1;
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (arr[mid] >= arr[left] && arr[mid] < arr[mid + 1]) left = mid + 1;
            else right = mid;
        }
        return left;
    }
};

//162. 寻找峰值

class Solution
{
public:
    int findPeakElement(vector<int>& nums)
    {
        int left = 0, right = nums.size() - 1;
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] >= nums[left] && nums[mid] < nums[mid + 1]) left = mid + 1;
            else right = mid;
        }
        return left;
    }
};

//153. 寻找旋转排序数组中的最小值


class Solution
{
public:
    int findMin(vector<int>& nums)
    {
        int left = 0, right = nums.size() - 1;
        if (nums[right] > nums[left]) return nums[0];
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            if (nums[mid] < nums[0]) right = mid;
            else left = mid + 1;
        }
        return nums[left];
    }
};