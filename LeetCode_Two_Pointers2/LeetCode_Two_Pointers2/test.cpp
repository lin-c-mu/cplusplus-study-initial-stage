#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>

//202. 快乐数

//快慢指针
class Solution
{
public:

    int totalSum(int n)
    {
        int num = n % 10, sum = 0;
        while (n)
        {
            sum += num * num;
            n /= 10;
            num = n % 10;
        }
        return sum;
    }
    bool isHappy(int n)
    {
        int slow = n, fast = totalSum(n);
        while (slow != fast)
        {
            slow = totalSum(slow);
            fast = totalSum(totalSum(fast));
        }
        if (slow == 1)
            return true;
        return false;
    }
};

//11. 盛最多水的容器

class Solution
{
public:
    int maxArea(vector<int>& height)
    {
        //主要利用单调性，定义左右指针，指针向内
        //：底恒变小，优化短高
        int left = 0, right = height.size() - 1, maxCapacity = 0;
        while (left < right)
        {
            int onceCapa = (right - left) * min(height[left], height[right]);
            maxCapacity = max(maxCapacity, onceCapa);
            if (height[left] < height[right])
                left++;
            else
                right--;
        }
        return maxCapacity;
    }
};

//611. 有效三角形的个数

class Solution
{
public:
    int triangleNumber(vector<int>& nums)
    {
        //1. 排序
        sort(nums.begin(), nums.end());
        //确定c边，控制a， b边范围
        int count = 0;
        for (int i = nums.size() - 1; i >= 2; --i)
        {
            int a = 0, b = i - 1;
            while (a < b)
            {
                if (nums[a] + nums[b] > nums[i])
                {
                    count += b - a;
                    --b;
                }
                else
                    ++a;
            }
        }
        return count;
    }
};

//15. 三数之和

class Solution
{
public:
    vector<vector<int>> threeSum(vector<int>& nums)
    {
        //造一个不重复的的数组 + 排序
        //vector<int> not_dup;
        sort(nums.begin(), nums.end());
        //while()
        //找正负分界数(第一个正数下标)
        int judge = nums.size() - 1;
        for (int i = 0; i < nums.size(); ++i)
        {
            if (nums[i] >= 0)
            {
                judge = i;
                break;
            }
        }
        //正负各一个，还有一个从右向左遍历
        vector<vector<int>> anr;
        int num = 0;
        for (int left = 0; left <= judge; ++left)
        {
            if ((left == 0) || nums[left] != nums[left - 1])
            {
                for (int right = nums.size() - 1; right >= judge; --right)
                {
                    if ((right + 1) == nums.size() || nums[right] != nums[right + 1])
                    {
                        for (int count = right - 1; count > left; --count)
                        {

                            if ((nums[left] + nums[right] + nums[count] < 0) || (nums[left] + nums[right] + nums[left + 1] > 0))
                            {
                                break;
                            }
                            else if (nums[left] + nums[right] + nums[count] == 0)
                            {
                                anr.push_back({ nums[left], nums[right], nums[count] });
                                num++;
                                break;
                            }
                        }
                    }

                }
            }

        }
        return anr;
    }
};