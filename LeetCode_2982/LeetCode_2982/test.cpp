#define  _CRT_SECURE_NO_WARNINGS 1

//2982. 找出出现至少三次的最长特殊子字符串 II


//方法一：二分查找
class Solution {
public:
    int maximumLength(string s) {
        int n = s.size();
        unordered_map<char, vector<int>> cnt;
        for (int i = 0, j = 0; i < s.size(); i = j) {
            while (j < s.size() && s[j] == s[i]) {
                j++;
            }
            cnt[s[i]].emplace_back(j - i);
        }

        int res = -1;
        for (auto& [_, vec] : cnt) {
            int lo = 1, hi = n - 2;
            while (lo <= hi) {
                int mid = (lo + hi) >> 1;
                int count = 0;
                for (int x : vec) {
                    if (x >= mid) {
                        count += x - mid + 1;
                    }
                }
                if (count >= 3) {
                    res = max(res, mid);
                    lo = mid + 1;
                }
                else {
                    hi = mid - 1;
                }
            }
        }
        return res;
    }
};

//方法二：一次遍历
class Solution {
public:
    int maximumLength(string s) {
        int n = s.size();
        vector<vector<int>> cnt(26, vector<int>(3));
        for (int i = 0, j = 0; i < s.size(); i = j) {
            while (j < s.size() && s[j] == s[i]) {
                j++;
            }
            int index = s[i] - 'a';
            int len = j - i;
            if (len > cnt[index][0]) {
                cnt[index][2] = cnt[index][1];
                cnt[index][1] = cnt[index][0];
                cnt[index][0] = len;
            }
            else if (len > cnt[index][1]) {
                cnt[index][2] = cnt[index][1];
                cnt[index][1] = len;
            }
            else if (len > cnt[index][2]) {
                cnt[index][2] = len;
            }
        }

        int res = 0;
        for (auto vec : cnt) {
            res = max({ res, vec[0] - 2, min(vec[0] - 1, vec[1]), vec[2] });
        }
        return res ? res : -1;
    }
};