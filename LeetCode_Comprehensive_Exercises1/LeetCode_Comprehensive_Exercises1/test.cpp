#define  _CRT_SECURE_NO_WARNINGS 1

//78. 子集

class Solution
{
    vector<vector<int>> ret;
    vector<int> path;
public:
    vector<vector<int>> subsets(vector<int>& nums)
    {
        dfs(nums, -1);
        return ret;
    }
    void dfs(vector<int>& nums, int priority)
    {
        ret.push_back(path);

        for (int i = 0; i < nums.size(); i++)
        {
            if (priority < i)
            {
                path.push_back(nums[i]);
                priority = i;
                dfs(nums, priority);
                //回溯 -》 恢复现场
                path.pop_back();
            }
        }
    }
};

//46. 全排列

class Solution
{
    vector<vector<int>> ret;
    vector<int> path;
    bool judge[7];
public:
    vector<vector<int>> permute(vector<int>& nums)
    {
        dfs(nums);
        return ret;
    }
    void dfs(vector<int>& nums)
    {
        //出口
        if (path.size() == nums.size())
        {
            ret.push_back(path);
            return;
        }
        for (int i = 0; i < nums.size(); i++)
        {
            //剪枝 - true 不处理
            if (judge[i] == false)
            {
                path.push_back(nums[i]);
                judge[i] = true;
                dfs(nums);
                //回溯 -> 恢复现场
                path.pop_back();
                judge[i] = false;
            }
        }
    }
};

//257. 二叉树的所有路径

class Solution
{

    vector<string> ret;
public:
    vector<string> binaryTreePaths(TreeNode* root)
    {
        string path;
        dfs(root, path);
        return ret;
    }
    void dfs(TreeNode* root, string path)
    {
        if (root == nullptr) return;
        path += to_string(root->val);
        if (root->left == nullptr && root->right == nullptr)
        {
            ret.push_back(path);
            return;
        }
        path += "->";
        dfs(root->left, path);
        dfs(root->right, path);
        return;
    }
};

//230. 二叉搜索树中第K小的元素

class Solution
{
    int count = 0;
    int ret = 0;
public:
    int kthSmallest(TreeNode* root, int k)
    {
        count = k;
        dfs(root);
        return ret;
    }
    void dfs(TreeNode* root)
    {
        if (root == nullptr) return;

        dfs(root->left);
        count--;
        if (count == 0) ret = root->val;
        dfs(root->right);
        return;
    }
};