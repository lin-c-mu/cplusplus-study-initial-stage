#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

//this指针问题

//class Date
//{
//public:
//	//void InitDate(Date* const this, int year = 2024, int month = 6, int day = 14)
//	void InitDate(int year = 2024, int month = 6, int day = 14)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	//void PrintDate(Date* const this);
//	void PrintDate()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1;
//	Date d2;
//	//d1.InitDate(&d1);
//	d1.InitDate();
//	d2.InitDate(2024, 6, 17);
//	d1.PrintDate();
//	d2.PrintDate();
//
//	return 0;
//}
//
//
//int main()
//{
//	int i = 1;
//	const int j = i;
//	const char* p = "hello";
//	cout << &i << endl;
//	cout << &j << endl;
//	cout << &p << endl;
//	cout << (void*)p << endl;
//	return 0;
//}
//
// // 1.下面程序编译运行结果是？  A、编译报错  B、运行崩溃  C、正常运行
//class A
//{
//public:
//    void PrintA()
//    {
//        cout << "PrintA()" << endl;
//		  cout << _a << endl;
//    }
//private:
//    int _a;
//};
//int main()
//{
//    A* p = nullptr;
//    p->PrintA();
//    (*p).PrintA();
//    return 0;
//}

//1. 构造函数

//class Date
//{
//public:
//	//1. 无参构造函数
//	Date()
//	{}
//
//	//2. 带参构造函数
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//void TestDate()
//{
//	Date d1; // 调用无参构造函数
//	Date d2(2024, 6, 19); // 调用带参的构造函数
//	// 注意：如果通过无参构造函数创建对象时，对象后面不用跟括号，否则就成了函数声明
//	// 以下代码的函数：声明了d3函数，该函数无参，返回一个日期类型的对象
//	// warning C4930: “Date d3(void)”: 未调用原型函数(是否是有意用变量定义的?)
//	Date d3();
//}

//class Date
//{
//public:
//	/*
//	// 如果用户显式定义了构造函数，编译器将不再生成
//   Date(int year, int month, int day)
//	{
//	_year = year;
//	_month = month;
//	_day = day;
//	}
//	*/
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	// 将Date类中构造函数屏蔽后，代码可以通过编译，因为编译器生成了一个无参的默认构造函
//   // 将Date类中构造函数放开，代码编译失败，因为一旦显式定义任何构造函数，编译器将不再
//   // 无参构造函数，放开后报错：error C2512: “Date”: 没有合适的默认构造函数可用
//	Date d1;
//	return 0;
//}

//class Time
//{
//public:
//	Time(int hour = 24, int minute = 0, int second = 0)
//	{
//		cout << "构造函数：Time()" << endl;
//		_hour = hour;
//		_minute = minute;
//		_second = second;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//public:
//
//	Date(int year = 2004, int month = 5, int day = 21)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	//基本类型（内置类型） -> 随机值
//	int _year;
//	int _month;
//	int _day;
//
//	//C++11以后版本，可以在成员变量声明时给默认值！！
//	//int _year = 2024;
//	//int _month = 6;
//	//int _day = 19;
//
//	//自定义类型  -> 调用自己的默认成员函数
//	Time _t;
//};
//
//int main()
//{
//	Date d1(2022);
//	d1.Print();
//	return 0;
//}

//class Date
//{
//public:
//	Date()
//		:_year(2024)
//		, _month(5)
//		, _day(9)
//	{}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//// 以下测试函数能通过编译吗？
//int main()
//{
//	Date d1;
//	return 0;
//}


//2. 析构函数

//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity = 3)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//	void Push(DataType data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//
//	// 其他方法...
//	
//	//析构函数 --- 释放动态开辟内存，防止内存泄露
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = NULL;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//
//private:
//	DataType* _array;
//	int _capacity;
//	int _size;
//}; 
//		
//void TestStack()
//{
//	Stack s;
//	s.Push(1);
//	s.Push(2);
//}
//
////class MyQueue
////{
////	//队列的初始化通过调用Mystack类的构造函数
////	//队列的销毁(释放堆)通过调用Mystack类的析构函数
////	MyStack st1;
////	MyStack st2;
////};
//
//int main()
//{
//	TestStack();
//	return 0;
//}

//class Time
//{
//public:
//	~Time()
//	{
//		cout << "~Time()" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//class Date
//{
//private:
//	// 基本类型(内置类型)
//	int _year = 1970;
//	int _month = 1;
//	int _day = 1;
//	// 自定义类型
//	Time _t;
//};
//int main()
//{
//	Date d;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int a)
//	{
//		_a = a;
//	}
//	~Date()
//	{
//		cout << "~Date() -> _a" << endl;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	Date d1(1);
//	Date d2(2);
//	static Date d3(3);
//
//	return 0;
//}

//3. 拷贝构造函数

//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	// Date(const Date d)   // 错误写法：编译报错，会引发无穷递归
//	//正确写法
//	Date(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	Date d2(d1);
//	return 0;
//}

//// 这里会发现下面的程序会崩溃掉？这就是浅拷贝的问题，具体如何解决以后讲
//class Stack
//{
//public:
//	Stack(size_t capacity = 10)
//	{
//		_array = (int*)malloc(sizeof(int) * capacity);
//		if (_array == nullptr)
//		{
//			perror("malloc fail:");
//			return;
//		}
//		
//		_capacity = capacity;
//		_size = 0;
//	}
//
//	//深拷贝
//	Stack(const Stack& s1)
//	{
//		int* tmp = (int*)malloc(s1._capacity * sizeof(int));
//		if (!tmp)
//		{
//			perror("malloc fail:");
//			exit(-1);
//		}
//
//		memcpy(tmp, s1._array, s1._size* sizeof(int));
//
//		_array = tmp;
//		_capacity = s1._capacity;
//		_size = s1._size;
//	}
//
//	void Push(int val)
//	{
//		//CheckCapacity();
//		_array[_size++] = val;
//	}
//
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = nullptr;
//			_capacity = _size = 0;
//		}
//	}
//
//private:
//	int* _array;
//	int _capacity;
//	int _size;
//};
//
//void TestStack()
//{
//	Stack s1;
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//	Stack s2(s1);
//}
//
//int main()
//{
//	TestStack();
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		cout << "Date(int,int,int):" << this << endl;
//	}
//	Date(const Date& d)
//	{
//		cout << "Date(const Date& d):" << this << endl;
//	}
//	~Date()
//	{
//		cout << "~Date():" << this << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//Date Test(Date d)
//{
//	Date temp(d);
//	return temp;
//}
//int main()
//{
//	Date d1(2024, 6, 22);
//	Test(d1);
//	return 0;
//}

//const 常引用

//void func(const int& a) {}
//
//int main()
//{
//	//b -> 只读； a -> 可读可写  --- 权限缩小
//	int a = 10;
//	const int& b = a;
//	//ax -> 只读； bx -> 可读可写  --- 权限放大 （报错）
//	const int ax = 10;
//	//int& bx = ax;
//
//	const int& m = 10;  // 常量
//	const int& n = a + b;  // 临时对象
//	//int& n = a + b; //权限放大
//
//	func(10);
//	func(a + b);
//
//	double d = 1.1;
//	int i = d;  //隐式类型转换
//	const int& ri = d;
//
//	return 0;
//}

//4. 赋值重载

class Date
{
public:
	Date(int year = 2024, int month = 6, int day = 23)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	bool operator==(const Date& d2)
	{
		return _year == d2._year 
			&& _month == d2._month
			&& _day == d2._day;
	}

	bool operator<(const Date& d2)
	{
		if (_year < d2._year)
			return true;
		else if (_year == d2._year)
		{
			if (_month < d2._month)
				return true;
			else if (_month == d2._month)
			{
				if (_day < d2._day)
					return true;
			}
		}
		return false;
	}

	//Date& operator=(const Date& d)
	//{
	//	if (this != &d)
	//	{
	//		_year = d._year;
	//		_month = d._month;
	//		_day = d._day;
	//	}
	//	return *this;
	//}
	

	bool DateEqual(const Date& d1, const Date& d2)
	{
		return d1._year == d2._year
			&& d1._month == d2._month
			&& d1._day == d2._day;
	}
//private:
	// 基本类型(内置类型)
	int _year ;
	int _month;
	int _day;
};

//Date& operator=(Date& left, const Date& right)
//{
//	if (&left != &right)
//	{
//		left._year = right._year;
//		left._month = right._month;
//		left._day = right._day;
//	}
//	return left;
//}

int main()
{
	Date d1(2024, 6, 23);
	Date d2(2023, 5, 22);

	cout << (d1 == d2) << endl; // d1.operator==(d2);
	cout << (d1 < d2) << endl;  // d1.operator<(d2);
	return 0;

}

