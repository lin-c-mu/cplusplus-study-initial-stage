#define  _CRT_SECURE_NO_WARNINGS 1

#include<iostream>

using namespace std;

#include<vector>
#include<unordered_map>

//300. 最长递增子序列

class Solution
{
public:
    int lengthOfLIS(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> dp(n, 1);
        int ans = 1;
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (nums[j] < nums[i])
                    dp[i] = max(dp[i], dp[j] + 1);
            }
            ans = max(ans, dp[i]);
        }
        return ans;
    }
};

//376. 摆动序列

class Solution
{
public:
    int wiggleMaxLength(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> f(n, 1);
        auto g = f;
        int ans = 1;
        //f->增； g->减
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (nums[j] < nums[i])
                    f[i] = max(f[i], g[j] + 1);
                if (nums[j] > nums[i])
                    g[i] = max(g[i], f[j] + 1);
                ans = max(ans, max(f[i], g[i]));
            }
        }
        return ans;
    }
};

//673. 最长递增子序列的个数

class Solution
{
public:
    int findNumberOfLIS(vector<int>& nums)
    {
        int n = nums.size();
        vector<int> len(n, 1);
        auto count = len;
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (nums[j] < nums[i])
                {
                    if (len[j] + 1 == len[i]) count[i] += count[j];
                    else if (len[j] + 1 > len[i]) len[i] = len[j] + 1, count[i] = count[j];
                }
            }
        }
        int Max = len[0], Count = count[0];
        for (int i = 1; i < n; i++)
        {
            if (Max == len[i]) Count += count[i];
            else if (Max < len[i]) Max = len[i], Count = count[i];
        }
        return Count;
    }
};

//646. 最长数对链

class Solution
{
public:
    int findLongestChain(vector<vector<int>>& pairs)
    {
        //先排序
        sort(pairs.begin(), pairs.end());

        int n = pairs.size();
        int ans = 1;
        vector<int> dp(n, 1);
        for (int i = 1; i < n; i++)
        {
            for (int j = 0; j < i; j++)
                if (pairs[j][1] < pairs[i][0]) dp[i] = max(dp[i], dp[j] + 1);
            ans = max(ans, dp[i]);
        }
        return ans;
    }
};

//1218. 最长定差子序列

//超时
class Solution
{
public:
    int longestSubsequence(vector<int>& arr, int difference)
    {
        int n = arr.size();
        vector<int> dp(n, 1);
        int ans = 1;
        for (int i = 1; i < n; i++)
        {
            for (int j = i - 1; j >= 0; j--)
            {
                if (arr[i] - arr[j] == difference)
                {
                    dp[i] = dp[j] + 1;
                    break;
                }
            }
            ans = max(ans, dp[i]);
        }
        return ans;
    }
};

//hash + 动态规划
class Solution
{
public:
    int longestSubsequence(vector<int>& arr, int difference)
    {
        unordered_map<int, int> hash; // arr[i] - dp[i]
        hash[arr[0]] = 1; // 初始化

        int ret = 1;
        for (int i = 1; i < arr.size(); i++)
        {
            hash[arr[i]] = hash[arr[i] - difference] + 1;
            ret = max(ret, hash[arr[i]]);
        }
        return ret;
    }
};